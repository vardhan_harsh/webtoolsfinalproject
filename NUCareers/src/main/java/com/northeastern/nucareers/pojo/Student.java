package com.northeastern.nucareers.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "student_table", uniqueConstraints = { @UniqueConstraint(columnNames = { "nuid" }) })
@PrimaryKeyJoinColumn(name = "userId")

public class Student extends User {

	public Student() {
		// TODO Auto-generated constructor stub
	}

	@Column(name = "firstName")
	private String firstName;
	@Column(name = "lastName")
	private String lastName;
	@Transient
	public static int viewCount = 0;
	private long nuid;
	private String gender;
	private String visaType;
	private float gpa;
	private String degree;
	private String major;
	private String preferedIndustry;
	private String workExperience;
	private String jobTitle;
	private String isAvailable;
	private String company;
	private String fileName;
	private String filePath;
	@Column(name = "phoneNumber")
	private long phoneNumber;

	@OneToMany(mappedBy = "student", orphanRemoval = true)
	@Cascade({ org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.DELETE })
	private Set<JobApplication> jobApplication = new HashSet<JobApplication>();

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getNuid() {
		return nuid;
	}

	public void setNuid(long nuid) {
		this.nuid = nuid;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getVisaType() {
		return visaType;
	}

	public void setVisaType(String visaType) {
		this.visaType = visaType;
	}

	public float getGpa() {
		return gpa;
	}

	public void setGpa(float gpa) {
		this.gpa = gpa;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getPreferedIndustry() {
		return preferedIndustry;
	}

	public void setPreferedIndustry(String preferedIndustry) {
		this.preferedIndustry = preferedIndustry;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String employer) {
		this.workExperience = employer;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(String availability) {
		this.isAvailable = availability;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Set<JobApplication> getJobApplication() {
		return jobApplication;
	}

	public void setJobApplication(Set<JobApplication> jobApplication) {
		this.jobApplication = jobApplication;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
