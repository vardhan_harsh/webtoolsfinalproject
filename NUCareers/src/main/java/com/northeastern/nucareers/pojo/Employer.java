package com.northeastern.nucareers.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "employer_table")
@PrimaryKeyJoinColumn(name = "userId")
public class Employer extends User {

	public Employer() {
		// TODO Auto-generated constructor stub
	}

	@Column(name = "firstName")
	private String firstName;
	@Column(name = "lastName")
	private String lastName;
	@Column(name = "contactCollege")
	private String contactCollege;
	@Column(name = "title")
	private String title;
	@Column(name = "phoneNumber")
	private long phoneNumber;
	@Column(name = "nuAlumnus")
	private String nuAlumnus;
	@OneToOne(mappedBy = "employer", cascade = CascadeType.ALL)
	private EmployerOrganization empOrg;
	@OneToOne(mappedBy = "employer", cascade = CascadeType.ALL)
	private EmployerDivision empDiv;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employer", orphanRemoval = true)
	@Cascade({ org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.DELETE })
	private Set<Job> jobs = new HashSet<Job>();

	public Employer(String firstName, String lastName, String contactCollege, String title, long phoneNumber,
			String nuAlumnus) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.contactCollege = contactCollege;
		this.title = title;
		this.phoneNumber = phoneNumber;
		this.nuAlumnus = nuAlumnus;
	}

	public Set<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Set<Job> jobs) {
		this.jobs = jobs;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactCollege() {
		return contactCollege;
	}

	public void setContactCollege(String contactCollege) {
		this.contactCollege = contactCollege;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNuAlumnus() {
		return nuAlumnus;
	}

	public void setNuAlumnus(String nuAlumnus) {
		this.nuAlumnus = nuAlumnus;
	}

	public EmployerOrganization getEmpOrg() {
		return empOrg;
	}

	public void setEmpOrg(EmployerOrganization empOrg) {
		this.empOrg = empOrg;
	}

	public EmployerDivision getEmpDiv() {
		return empDiv;
	}

	public void setEmpDiv(EmployerDivision empDiv) {
		this.empDiv = empDiv;
	}

}
