package com.northeastern.nucareers.pojo;

public class States {
	public States() {
		// TODO Auto-generated constructor stub
	}

	private String code;
	private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
