package com.northeastern.nucareers.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "emp_organization_table")
public class EmployerOrganization {
	public EmployerOrganization() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "employer"))
	@Column(name = "empOrgId", unique = true, nullable = false)
	private long id;
	@Column(name = "organizationName")
	private String organizationName;
	@Column(name = "industry")
	private String industry;
	@Column(name = "website")
	private String website;
	@Column(name = "description")
	private String description;

	@OneToOne
	@PrimaryKeyJoinColumn
	private Employer employer;

	public EmployerOrganization(String organizationName, String industry, String website, String description) {

		this.organizationName = organizationName;
		this.industry = industry;
		this.website = website;
		this.description = description;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

}
