package com.northeastern.nucareers.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.northeastern.nucareers.dao.EmailDAO;
import com.northeastern.nucareers.dao.JobApplicationDAO;
import com.northeastern.nucareers.dao.StudentDAO;
import com.northeastern.nucareers.pojo.JobApplication;
import com.northeastern.nucareers.pojo.Student;
import com.northeastern.nucareers.pojo.User;

@Controller
@RequestMapping(value = "/**/student/*")
public class StudentProfileController {

	@Autowired
	@Qualifier("studentDAO")
	StudentDAO studentDAO;

	@Autowired
	@Qualifier("mail")
	EmailDAO mail;

	@Autowired
	@Qualifier("jobAppDao")
	JobApplicationDAO jobAppDao;

	@RequestMapping(value = "/viewprofile.htm", method = RequestMethod.GET)
	public String viewProfile(@ModelAttribute("student") Student student, HttpServletRequest request,
			HttpServletResponse response, Model model) {

		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		try {
			Student stud = studentDAO.viewProfile(user.getUserName());
			model.addAttribute("student", stud);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		return "student-profile";
	}

	@RequestMapping(value = "/employer/student/viewprofile.htm", method = RequestMethod.POST)
	public String viewStudentProfile(@ModelAttribute("student") Student student, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		int applicationId = Integer.parseInt(request.getParameter("appId"));
		request.setAttribute("applicationId", applicationId);

		Student stud = null;

		try {
			String userName = request.getParameter("userName");
			stud = studentDAO.viewProfile(userName);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		for (JobApplication j : stud.getJobApplication()) {
			if (j.getJobApplicationId() == applicationId) {
				model.addAttribute("appStatus", j.getStatus());
				break;
			}
		}
		model.addAttribute("student", stud);
		return "employer-decision-view";
	}

}
