package com.northeastern.nucareers.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.northeastern.nucareers.dao.JobApplicationDAO;
import com.northeastern.nucareers.dao.JobDAO;
import com.northeastern.nucareers.dao.StudentDAO;
import com.northeastern.nucareers.dao.UtilityRestDAO;
import com.northeastern.nucareers.pojo.Job;
import com.northeastern.nucareers.pojo.JobApplication;
import com.northeastern.nucareers.pojo.Student;
import com.northeastern.nucareers.pojo.User;

@Controller
@RequestMapping(value = "/student/*")
public class StudentController {

	@Autowired
	ServletContext context;

	@Autowired
	@Qualifier("studentDAO")
	StudentDAO studentDAO;

	@Autowired
	@Qualifier("restDao")
	UtilityRestDAO restDao;

	@Autowired
	@Qualifier("jobDao")
	JobDAO jobDao;

	@Autowired
	@Qualifier("jobAppDao")
	JobApplicationDAO jobAppDao;

	private static final int BUFFER_SIZE = 4096;

	private static final Logger logger = LoggerFactory.getLogger(Student.class);

	@RequestMapping(value = "/student/dashboard.htm", method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		return "student-dashboard";
	}

	@RequestMapping(value = "/student/editprofile.htm", method = RequestMethod.GET)
	public String viewEditStudentProfile(HttpServletRequest request, Model model) throws Exception {

		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		try {
			Student stud = studentDAO.viewProfile(user.getUserName());
			List<String> lstCountries = restDao.getCountryList();
			List<String> stateList = restDao.getStateListByCountry("US");
			model.addAttribute("stateList", stateList);
			model.addAttribute("countryList", lstCountries);
			model.addAttribute("student", stud);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		return "student-edit-profile";
	}

	@RequestMapping(value = "/student/editprofile.htm", method = RequestMethod.POST)
	public String editStudentProfile(@ModelAttribute("student") Student student, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		try {
			System.out.println("*********Inside editStudentProfile");
			System.out.println(student.getFirstName());

			System.out.println(user.getUserName());
			Student stud = studentDAO.updateStudentProfile(student);
			model.addAttribute("student", stud);
			return "student-profile";
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
	}

	@RequestMapping(value = "/student/editportfolio.htm", method = RequestMethod.GET)
	public String editStudentporfolio(HttpServletRequest request, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		try {
			Student stud = studentDAO.viewProfile(user.getUserName());
			model.addAttribute("student", stud);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		return "student-edit-portfolio";
	}

	@RequestMapping(value = "/student/uploadresume.htm", method = RequestMethod.POST)
	public String uploadResume(@ModelAttribute("student") Student student, @RequestParam("name") String name,
			@RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response,
			Model model) {

		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + name + "."
						+ FilenameUtils.getExtension(file.getOriginalFilename()));
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				String path = serverFile.getAbsolutePath();
				System.out.println(path);

				logger.info("Server File Location=" + path);
				Student stud = studentDAO.uploadResume(user.getUserName(), path, name);
				model.addAttribute("student", stud);
				request.setAttribute("name", name);

				HttpSession session = request.getSession();
				session.setAttribute("path", path);
				session.setAttribute("name", name);

				return "student-edit-portfolio";
			} catch (Exception e) {
				model.addAttribute("errorMessage", "You failed to upload " + name + " => " + e.getMessage());
				return "error";
			}
		} else {
			model.addAttribute("errorMessage", "You failed to upload " + name + " because the file was empty.");
			return "student-edit-portfolio";
		}
	}

	@RequestMapping(value = "/student/download.htm", method = RequestMethod.GET)
	public void downloadResume(@RequestParam("name") String filePath, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// get absolute path of the application

		System.out.println("File path is: " + filePath);

		// construct the complete absolute path of the file

		File downloadFile = new File(filePath);
		FileInputStream inputStream = new FileInputStream(downloadFile);

		// get MIME type of the file
		String mimeType = context.getMimeType(filePath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}
		System.out.println("MIME type: " + mimeType);

		// set content attributes for the response
		response.setContentType(mimeType);
		response.setContentLength((int) downloadFile.length());

		OutputStream outStream = response.getOutputStream();

		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead = -1;

		// write bytes read from the input stream into the output stream
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, bytesRead);
		}
		inputStream.close();
		outStream.close();
	}

	@RequestMapping(value = "/student/submitportfolio.htm", method = RequestMethod.POST)
	public String submitPortfolio(@ModelAttribute("student") Student student, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		try {
			Student stud = studentDAO.submitPortfolio(user.getUserName(), student.getDegree(), student.getMajor(),
					student.getGpa(), student.getPreferedIndustry(), student.getWorkExperience());
			model.addAttribute("student", stud);
			model.addAttribute("message", "Profile updated successfully.");
			return "student-profile";
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";

		}
	}

	@RequestMapping(value = "/student/viewjobs.htm", method = RequestMethod.GET)
	protected String handleRequestInternal(HttpServletRequest request, HttpServletResponse response, Model model)
			throws Exception {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		List<Job> jobList = new ArrayList<Job>();
		try {
			jobList = jobDao.getJobList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		model.addAttribute("jobList", jobList);
		return "student-view-job";
	}

	@RequestMapping(value = "/student/viewjobdetail.htm", method = RequestMethod.POST)
	protected String viewJob(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		try {
			Job job = new Job();
			int jobId = Integer.parseInt(request.getParameter("jobId"));
			job = jobDao.viewJobDetail(jobId);
			JobApplication jobapplication = null;
			jobapplication = jobAppDao.getStudentJobApplication(user.getUserName(), jobId);
			Student student = new Student();
			student = studentDAO.viewProfile(user.getUserName());
			model.addAttribute("jobapp", jobapplication);
			model.addAttribute("job", job);
			model.addAttribute("studentCompany", student.getCompany());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			/*
			 * model.addAttribute("catchError", e.getMessage()); return "error";
			 */
		}
		return "student-view-jobdetail";

	}

	@RequestMapping(value = "/student/viewjobapplication.htm", method = RequestMethod.GET)
	protected String viewJobApplication(HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		List<JobApplication> appList = new ArrayList<JobApplication>();

		try {
			appList = jobAppDao.getStudentJobApplicationList(user.getUserName());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		model.addAttribute("appList", appList);
		if (request.getParameter("status") != null && request.getParameter("status").equalsIgnoreCase("applied"))
			model.addAttribute("message", "Successfully applied to the job");
		return "student-view-jobapplication";
	}

	@RequestMapping(value = "/student/filterjob.htm", method = RequestMethod.POST)
	protected String filterJob(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		List<Job> jobList = new ArrayList<Job>();
		try {
			String inputJobTitle = request.getParameter("inputJobTitle");
			String jobType = request.getParameter("jobType");
			jobList = jobDao.viewJobsByFilters(inputJobTitle, jobType);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		if (jobList.isEmpty())
			model.addAttribute("filterResult",
					"Please try a different job title/type combination or broaden your search criteria.");
		model.addAttribute("jobList", jobList);
		return "student-view-job";

	}

	@RequestMapping(value = "/student/viewJobOffers.htm", method = RequestMethod.GET)
	protected String viewJobOffers(HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		List<JobApplication> appList = new ArrayList<JobApplication>();

		try {
			appList = jobAppDao.jobApplicationsOffered(user.getUserName());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		if (request.getParameter("status") != null && request.getParameter("status").equalsIgnoreCase("accepted"))
			model.addAttribute("message", "Congratulation on accepting the offer.");
		model.addAttribute("appList", appList);
		return "student-job-offers";
	}

	@RequestMapping(value = "/student/studentdecision.htm", method = RequestMethod.GET)
	protected String jobOfferDecision(HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		// JobApplication jobapp = new JobApplication();
		System.out.println("----------------------INSIDE SUTDENT DECISION");
		try {
			int jobApplicationId = Integer.parseInt(request.getParameter("jobapp"));
			String decision = request.getParameter("decision");
			jobAppDao.setJobofferDecision(user.getUserName(), jobApplicationId, decision);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		// model.addAttribute("message", "Congratulation on accepting the offer.");
		// model.addAttribute("appList", jobapp);
		return "redirect:/student/viewJobOffers.htm?status=accepted";
	}

}
