package com.northeastern.nucareers.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.northeastern.nucareers.dao.JobApplicationDAO;
import com.northeastern.nucareers.dao.JobDAO;
import com.northeastern.nucareers.pojo.Job;
import com.northeastern.nucareers.pojo.JobApplication;
import com.northeastern.nucareers.pojo.User;

@Controller
@RequestMapping(value = "/student/*")
public class JobApplicationController {

	@Autowired
	@Qualifier("jobDao")
	JobDAO jobDao;

	@Autowired
	@Qualifier("jobAppDao")
	JobApplicationDAO jobAppDao;

	@RequestMapping(value = "/student/applyjob.htm", method = RequestMethod.POST)
	public String applyJob(@ModelAttribute("application") JobApplication application, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		try {
			int jobId = Integer.parseInt(request.getParameter("jobId"));
			Job job = jobDao.viewJobDetail(jobId);
			model.addAttribute("job", job);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}

		return "student-job-appplicationfrom";
	}

	@RequestMapping(value = "/student/submitjobapplication.htm", method = RequestMethod.POST)
	public String submitJobApplication(@ModelAttribute("application") JobApplication application, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		try {
			System.out.println(user.getUserName());
			int jobId = Integer.parseInt(request.getParameter("jobId"));
			jobAppDao.createJobApplication(user.getUserName(), jobId, application.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		return "redirect:/student/viewjobapplication.htm?status=applied";

	}
}
