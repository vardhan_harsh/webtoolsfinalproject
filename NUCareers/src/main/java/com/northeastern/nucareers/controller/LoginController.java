package com.northeastern.nucareers.controller;

import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.captcha.botdetect.web.servlet.Captcha;
import com.northeastern.nucareers.dao.EmailDAO;
import com.northeastern.nucareers.dao.EmployerDAO;
import com.northeastern.nucareers.dao.UserDAO;
import com.northeastern.nucareers.dao.UtilityRestDAO;
import com.northeastern.nucareers.pojo.Employer;
import com.northeastern.nucareers.pojo.Student;
import com.northeastern.nucareers.pojo.User;

@Controller
@RequestMapping(value = "/login/*")
public class LoginController {

	@Autowired
	@Qualifier("restDao")
	UtilityRestDAO restDao;

	@Autowired
	@Qualifier("employerDao")
	EmployerDAO employerDao;

	@Autowired
	@Qualifier("mail")
	EmailDAO mail;

	@RequestMapping(value = "/login/login.htm", method = RequestMethod.GET)
	public String showLoginForm(HttpServletRequest request, UserDAO userDAO, Model map) {
		Exception exception = (Exception) request.getSession().getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
		if (exception instanceof BadCredentialsException)
			map.addAttribute("errorMessage", "Invalid username/password");
		if (request.getUserPrincipal() != null) {
			HttpSession session = (HttpSession) request.getSession();
			User u = userDAO.getByUsername(request.getUserPrincipal().getName());

			if (u != null && u.getStatus() == 1) {
				session.setAttribute("user", u);
				if (u.getUserType().equalsIgnoreCase("ROLE_STUDENT")) {
					return "redirect:/student/dashboard.htm";
				} else if (u.getUserType().equalsIgnoreCase("ROLE_EMPLOYER")) {
					return "redirect:/employer/dashboard.htm";
				}
			} else if (u != null && u.getStatus() == 0) {
				map.addAttribute("errorMessage", "Please activate your account to login!");
				return "error";
			} else {
				map.addAttribute("errorMessage", "Invalid username/password");
				System.out.println("Got the error values");
				return "user-login";
			}
		}
		return "user-login";
	}

	@RequestMapping(value = "/login/error.htm", method = RequestMethod.GET)
	public String auth(HttpServletRequest request, UserDAO userDAO, Model map) {
		map.addAttribute("catchError", "You are not authorized to access the link.");
		return "error";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String loginFailure(HttpServletRequest request, UserDAO userDAO, Model map) {
		map.addAttribute("errorMessage", "Invalid username/password");
		System.out.println("Got the error values");
		return "user-login";
	}

	// @RequestMapping(value = "/login.htm", method = RequestMethod.POST)
	// public String handleLoginForm(HttpServletRequest request, UserDAO userDao,
	// ModelMap map) {
	// String username = request.getParameter("username");
	// String password = request.getParameter("password");
	// HttpSession session = (HttpSession) request.getSession();
	// try {
	// User u = userDao.get(username, password);
	//
	// if (u != null && u.getStatus() == 1) {
	// session.setAttribute("user", u);
	// if (u.getUserType().equalsIgnoreCase("ROLE_STUDENT")) {
	// return "redirect:/student/dashboard.htm";
	// } else if (u.getUserType().equalsIgnoreCase("ROLE_EMPLOYER")) {
	// return "redirect:/employer/dashboard.htm";
	// }
	// } else if (u != null && u.getStatus() == 0) {
	// map.addAttribute("errorMessage", "Please activate your account to login!");
	// return "error";
	// } else {
	// map.addAttribute("errorMessage", "Invalid username/password");
	// System.out.println("Got the error values");
	// return "user-login";
	// }
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// map.addAttribute("catchError", e.getMessage());
	// return "error";
	// }
	//
	// return null;
	//
	// }

	@RequestMapping(value = "/register.htm", method = RequestMethod.GET)
	public String showRegisterType() {

		return "register-user";
	}

	@RequestMapping(value = "/create.htm", method = RequestMethod.GET)
	public String showCreateForm(HttpServletRequest request, Model model) {

		String type = request.getParameter("type");
		if (type.equalsIgnoreCase("student")) {
			List<String> lstCountries = restDao.getCountryList();
			List<String> stateList = restDao.getStateListByCountry("US");
			model.addAttribute("stateList", stateList);
			model.addAttribute("countryList", lstCountries);
			model.addAttribute("student", new Student());
			return "student-register";
		} else if (type.equalsIgnoreCase("employer")) {
			List<String> lstCountries = restDao.getCountryList();
			List<String> stateList = restDao.getStateListByCountry("US");
			model.addAttribute("stateList", stateList);
			model.addAttribute("countryList", lstCountries);
			model.addAttribute("employer", new Employer());
			return "employer-register";
		} else {
			model.addAttribute("catchError", "Select valid user type");
			return "error";
		}
	}

	@RequestMapping(value = "/login/createEmployer.htm", method = RequestMethod.POST)
	public String registerEmployer(HttpServletRequest request, @ModelAttribute("employer") Employer employer,
			UserDAO userDao, ModelMap map) {
		Captcha captcha = Captcha.load(request, "CaptchaObject");
		String captchaCode = request.getParameter("captchaCode");
		HttpSession session = request.getSession();
		if (captcha.validate(captchaCode)) {
			employer.setUserType("ROLE_EMPLOYER");
			employer.setUserName(employer.getEmail());
			employer.setStatus(0);
			// CODE Change need for EMPLOYER
			try {
				userDao.registerEmployer(employer);
				Random rand = new Random();
				int randomNum1 = rand.nextInt(5000000);
				int randomNum2 = rand.nextInt(5000000);
				try {
					String str = "http://localhost:8080/nucareers/login/validateemail.htm?email=" + employer.getEmail()
							+ "&key1=" + randomNum1 + "&key2=" + randomNum2;
					session.setAttribute("key1", randomNum1);
					session.setAttribute("key2", randomNum2);

					mail.sendEmail(employer.getEmail(), "Click on this link to activate your account : " + str);
				} catch (Exception e) {
					System.out.println("Email cannot be sent");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				map.addAttribute("catchError", e.getMessage());
				return "error";
			}
		} else {
			map.addAttribute("errorMessage", "Invalid Captcha!");
			return "employer-register";
		}
		map.addAttribute("message",
				"You are successfully registered. Please check your registered email to activate your account.");
		return "user-login";
	}

	@RequestMapping(value = "/login/createStudent.htm", method = RequestMethod.POST)
	public String registerStudent(HttpServletRequest request, @ModelAttribute("student") Student student,
			UserDAO userDao, ModelMap map) {
		Captcha captcha = Captcha.load(request, "CaptchaObject");
		String captchaCode = request.getParameter("captchaCode");
		HttpSession session = request.getSession();
		if (captcha.validate(captchaCode)) {
			student.setUserType("ROLE_STUDENT");
			student.setStatus(0);
			// CODE Change need for EMPLOYER
			try {
				userDao.registerStudent(student);
				Random rand = new Random();
				int randomNum1 = rand.nextInt(5000000);
				int randomNum2 = rand.nextInt(5000000);
				try {
					String str = "http://localhost:8080/nucareers/login/validateemail.htm?email=" + student.getEmail()
							+ "&key1=" + randomNum1 + "&key2=" + randomNum2;
					session.setAttribute("key1", randomNum1);
					session.setAttribute("key2", randomNum2);

					mail.sendEmail(student.getEmail(), "Click on this link to activate your account : " + str);
				} catch (Exception e) {
					System.out.println("Email cannot be sent");
					map.addAttribute("catchError", "Email cannot be sent. " + e.getMessage());
					return "error";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				map.addAttribute("catchError", e.getMessage());
				return "error";
			}
		} else {
			map.addAttribute("errorMessage", "Invalid Captcha!");
			return "faculty-register";
		}
		map.addAttribute("message",
				"You are successfully registered. Please check your registered email to activate your account.");
		return "user-login";
	}

	@RequestMapping(value = "/login/forgotpassword.htm", method = RequestMethod.GET)
	public String getForgotPasswordForm(HttpServletRequest request) {

		return "forgot-password";
	}

	@RequestMapping(value = "/login/forgotpassword.htm", method = RequestMethod.POST)
	public String handleForgotPasswordForm(HttpServletRequest request, UserDAO userDao, Model model) {

		String useremail = request.getParameter("useremail");
		Captcha captcha = Captcha.load(request, "CaptchaObject");
		String captchaCode = request.getParameter("captchaCode");

		if (captcha.validate(captchaCode)) {
			User user = userDao.get(useremail);
			mail.sendEmail(useremail, "Your password is : " + user.getPassword());
			model.addAttribute("message", "Your password is sent to your registered email.");
			return "user-login";
		} else {
			model.addAttribute("errorMessage", "Invalid capcha");
			return "forgot-password";
		}
	}

	@RequestMapping(value = "login/resendemail.htm", method = RequestMethod.POST)
	public String resendEmail(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		String useremail = request.getParameter("useremail");
		Random rand = new Random();
		int randomNum1 = rand.nextInt(5000000);
		int randomNum2 = rand.nextInt(5000000);
		try {
			String str = "http://localhost:8080/nucareers/login/validateemail.htm?email=" + useremail + "&key1="
					+ randomNum1 + "&key2=" + randomNum2;
			session.setAttribute("key1", randomNum1);
			session.setAttribute("key2", randomNum2);
			mail.sendEmail(useremail, "Click on this link to activate your account : " + str);
		} catch (Exception e) {
			System.out.println("Email cannot be sent");
			model.addAttribute("catchError", "Email cannot be sent. " + e.getMessage());
			return "error";
		}

		return "user-created";
	}

	@RequestMapping(value = "login/validateemail.htm", method = RequestMethod.GET)
	public String validateEmail(HttpServletRequest request, UserDAO userDao, ModelMap map) {

		// The user will be sent the following link when the use registers
		// This is the format of the email
		// http://hostname:8080/lab10/user/validateemail.htm?email=useremail&key1=<random_number>&key2=<body
		// of the email that when user registers>
		HttpSession session = request.getSession();
		String email = request.getParameter("email");
		int key1 = Integer.parseInt(request.getParameter("key1"));
		int key2 = Integer.parseInt(request.getParameter("key2"));
		System.out.println(session.getAttribute("key1"));
		System.out.println(session.getAttribute("key2"));

		if ((Integer) (session.getAttribute("key1")) == key1 && ((Integer) session.getAttribute("key2")) == key2) {
			try {
				System.out.println("HI________");
				boolean updateStatus = userDao.updateUser(email);
				if (updateStatus) {
					map.addAttribute("message", "Your account is active.");
					return "user-login";
				} else {

					return "register-link-error";
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				map.addAttribute("catchError", e.getMessage());
				return "error";
			}
		} else {
			map.addAttribute("errorMessage", "Link expired , generate new link");
			map.addAttribute("resendLink", true);
			return "register-link-error";
		}
	}

}
