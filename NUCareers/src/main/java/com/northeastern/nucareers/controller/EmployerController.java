package com.northeastern.nucareers.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.northeastern.nucareers.dao.EmailDAO;
import com.northeastern.nucareers.dao.EmployerDAO;
import com.northeastern.nucareers.dao.JobApplicationDAO;
import com.northeastern.nucareers.dao.JobDAO;
import com.northeastern.nucareers.dao.UserDAO;
import com.northeastern.nucareers.dao.UtilityRestDAO;
import com.northeastern.nucareers.pojo.Employer;
import com.northeastern.nucareers.pojo.Job;
import com.northeastern.nucareers.pojo.JobApplication;
import com.northeastern.nucareers.pojo.User;

@Controller
@RequestMapping(value = "/employer/*")
public class EmployerController {

	@Autowired
	@Qualifier("employerDao")
	EmployerDAO employerDao;

	@Autowired
	@Qualifier("jobDao")
	JobDAO jobDao;

	@Autowired
	@Qualifier("userDao")
	UserDAO userDao;

	@Autowired
	@Qualifier("restDao")
	UtilityRestDAO restDao;

	@Autowired
	@Qualifier("jobAppDao")
	JobApplicationDAO jobAppDao;

	@Autowired
	@Qualifier("mail")
	EmailDAO mail;

	@RequestMapping(value = "/employer/dashboard.htm", method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		return "employer-dashboard";
	}

	@RequestMapping(value = "/employer/profile.htm", method = RequestMethod.GET)
	public String viewProfile(HttpServletRequest request, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		try {
			Employer employer = employerDao.viewProfile(user.getUserName());
			model.addAttribute("employer", employer);
		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		return "employer-profile";
	}

	@RequestMapping(value = "/employer/editprofile.htm", method = RequestMethod.GET)
	public String editProfile(HttpServletRequest request, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		try {
			Employer employer = employerDao.viewProfile(user.getUserName());
			List<String> lstCountries = restDao.getCountryList();
			List<String> stateList = restDao.getStateListByCountry("US");
			model.addAttribute("stateList", stateList);
			model.addAttribute("countryList", lstCountries);
			model.addAttribute("employer", employer);
		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		return "employer-edit-profile";
	}

	@RequestMapping(value = "/employer/updateprofile.htm", method = RequestMethod.POST)
	public String updateProfile(HttpServletRequest request, @ModelAttribute("employer") Employer employer,
			ModelMap map) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		try {
			employer.setUserType("employer");
			employer.setUserName(employer.getEmail());
			employer.setStatus(0);
			employerDao.updateEmployer(employer);
			map.addAttribute("employer", employer);
		} catch (Exception e) {
			// TODO: handle exception
			map.addAttribute("catchError", e.getMessage());
			return "error";
		}
		return "employer-profile";
	}

	@RequestMapping(value = "/employer/jobs.htm", method = RequestMethod.GET)
	public String viewJobs(HttpServletRequest request, @ModelAttribute("job") Job job, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		List<Job> jobList = new ArrayList<Job>();
		try {
			jobList = jobDao.getJobListById(user.getUserName());
			System.out.println(Arrays.toString(jobList.toArray()));

		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		model.addAttribute("jobList", jobList);
		return "employer-jobs";
	}

	@RequestMapping(value = "/employer/jobapplications.htm", method = RequestMethod.GET)
	public String viewJobApplication(HttpServletRequest request, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		List<JobApplication> appList = new ArrayList<JobApplication>();

		try {
			appList = jobAppDao.getEmpAppList(user.getUserName());
			System.out.println(appList);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}

		model.addAttribute("empAppList", appList);
		return "employer-jobs-applications";
	}

	@RequestMapping(value = "/employer/postjob.htm", method = RequestMethod.GET)
	public String postJob(HttpServletRequest request, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		model.addAttribute("job", new Job());
		return "employer-post-job";
	}

	@RequestMapping(value = "/employer/postjob.htm", method = RequestMethod.POST)
	public String submitPostJob(HttpServletRequest request, @ModelAttribute("job") Job job, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}

		try {
			jobDao.createJob(user.getUserName(), job);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		model.addAttribute("message", "Job posted successfully");
		return "employer-post-job";
	}

	@RequestMapping(value = "/employer/jobDetails.htm", method = RequestMethod.POST)
	public String viewJobDetails(HttpServletRequest request, Model model) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		Job job = null;
		try {
			int jobId = Integer.parseInt(request.getParameter("jobId"));
			job = jobDao.viewJobDetail(jobId);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		model.addAttribute("job", job);
		return "employer-job-detail";
	}

	@RequestMapping(value = "/employer/updatejob.htm", method = RequestMethod.POST)
	public String updateJobDetails(HttpServletRequest request, Model model, @ModelAttribute("job") Job job) {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		Job updatedJob = new Job();
		try {
			int jobId = Integer.parseInt(request.getParameter("jobId"));
			updatedJob = jobDao.updateJobDetails(user.getUserName(), job, jobId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		model.addAttribute("job", updatedJob);
		return "employer-job-detail";
	}

	@RequestMapping(value = "/employer/deletejob.htm", method = RequestMethod.GET)
	protected String deleteJob(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		try {
			int jobId = Integer.parseInt(request.getParameter("jobId"));
			System.out.println("Job ID is: " + jobId);
			jobDao.deleteJob(jobId);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}

		return "redirect:/employer/jobs.htm";
	}

	@RequestMapping(value = "/employer/decision.htm", method = RequestMethod.POST)
	public String makeDecisions(HttpServletRequest request, HttpServletResponse response, Model model)
			throws Exception {
		User user = ((User) request.getSession().getAttribute("user"));
		if (user == null) {
			return "redirect:/login/login.htm";
		} else if (user.getUserName() == null) {
			return "redirect:/login/login.htm";
		}
		System.out.println("******Decision Making**********");
		int applicationId = Integer.parseInt(request.getParameter("applicationId"));
		String empmessage = request.getParameter("empmessage");
		System.out.println(empmessage);
		String status = request.getParameter("status");
		System.out.println(status);

		try {
			JobApplication jobApp = jobAppDao.setDecision(applicationId, empmessage, status);
			String applicantEmail = request.getParameter("email");
			String firstName = request.getParameter("firstname");
			Employer emp = (Employer) user;
			mail.sendResponseMail(user.getEmail(), emp.getFirstName() + " " + emp.getLastName(),
					emp.getEmpOrg().getOrganizationName(), firstName, applicantEmail, empmessage, status,
					"Your application for " + jobApp.getJob().getJobCategory() + " at "
							+ emp.getEmpOrg().getOrganizationName());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			model.addAttribute("catchError", e.getMessage());
			return "error";
		}
		return "redirect:/employer/jobapplications.htm";
	}
}
