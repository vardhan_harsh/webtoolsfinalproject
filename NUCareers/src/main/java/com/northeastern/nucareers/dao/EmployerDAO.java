package com.northeastern.nucareers.dao;

import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;

import com.northeastern.nucareers.pojo.Employer;
import com.northeastern.nucareers.pojo.User;

public class EmployerDAO extends DAO {
	public EmployerDAO() {
		// TODO Auto-generated constructor stub
	}

	public Employer getEmployer(long id) throws Exception {
		try {
			begin();
			TypedQuery<Employer> q = getSession().createQuery("from Employer id= :id", Employer.class);
			q.setParameter("id", id);
			Employer emp = (Employer) q.getSingleResult();
			commit();
			return emp;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get user ", e);
		}
	}

	public Employer viewProfile(String userName) throws Exception {
		// TODO Auto-generated method stub
		try {
			begin();
			System.out.println("Inside profile DAO");

			System.out.println(userName);
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = (User) query.getSingleResult();
			TypedQuery<Employer> q2 = getSession().createQuery("From Employer where userId=:userId", Employer.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());
			Employer employer = (Employer) q2.getSingleResult();
			commit();
			return employer;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating student: " + e.getMessage());
		}
	}

	public Employer updateEmployer(Employer emp) throws Exception {
		try {
			begin();
			System.out.println("Inside Emp DAO");

			System.out.println(emp.getUserName());
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", emp.getUserName());
			User userAccount = (User) query.getSingleResult();
			TypedQuery<Employer> q2 = getSession().createQuery("From Employer where userId=:userId", Employer.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());
			Employer employer = (Employer) q2.getSingleResult();

			employer.setFirstName(emp.getFirstName());
			employer.setLastName(emp.getLastName());
			employer.setTitle(emp.getTitle());
			employer.setPhoneNumber(emp.getPhoneNumber());
			employer.setNuAlumnus(emp.getNuAlumnus());
			employer.setPassword(emp.getPassword());

			employer.getAddress().setAddressLine1(emp.getAddress().getAddressLine1());
			employer.getAddress().setAddressLine2(emp.getAddress().getAddressLine2());
			employer.getAddress().setAddressLine3(emp.getAddress().getAddressLine3());
			employer.getAddress().setCity(emp.getAddress().getCity());
			employer.getAddress().setCountry(emp.getAddress().getCountry());
			employer.getAddress().setState(emp.getAddress().getState());
			employer.getAddress().setZipCode(emp.getAddress().getZipCode());

			employer.getEmpDiv().setWebsite(emp.getEmpDiv().getWebsite());
			employer.getEmpDiv().setDescription(emp.getEmpDiv().getDescription());

			employer.getEmpOrg().setWebsite(emp.getEmpOrg().getWebsite());
			employer.getEmpOrg().setDescription(emp.getEmpOrg().getDescription());

			getSession().update(employer);
			commit();
			return employer;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating employer: " + e.getMessage());
		}

	}
}
