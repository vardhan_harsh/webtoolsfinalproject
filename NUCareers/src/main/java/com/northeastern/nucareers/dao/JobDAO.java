package com.northeastern.nucareers.dao;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.northeastern.nucareers.pojo.Employer;
import com.northeastern.nucareers.pojo.Job;
import com.northeastern.nucareers.pojo.User;

public class JobDAO extends DAO {

	public List<Job> getJobListById(String username) throws Exception {
		try {
			begin();
			System.out.println("Inside get job List By ID DAO");

			System.out.println(username);
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", username);
			User userAccount = (User) query.getSingleResult();
			TypedQuery<Job> q2 = getSession().createQuery("From Job where employer_id=:userId", Job.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());
			List<Job> listJob = q2.getResultList();
			commit();
			return listJob;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating student: " + e.getMessage());
		}
	}

	public Job createJob(String userName, Job job) throws Exception {
		try {
			begin();
			System.out.println("Inside Emp DAO");

			System.out.println(userName);
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = (User) query.getSingleResult();
			TypedQuery<Employer> q2 = getSession().createQuery("From Employer where userId=:userId", Employer.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());
			Employer employer = (Employer) q2.getSingleResult();

			Job jobs = new Job();
			jobs.setJobTitle(job.getJobTitle());
			jobs.setJobDesc(job.getJobDesc());
			jobs.setJobType(job.getJobType());
			jobs.setJobCategory(job.getJobCategory());
			jobs.setJobLocation(job.getJobLocation());
			jobs.setSalary(job.getSalary());
			jobs.setExperienceLevel(job.getExperienceLevel());
			jobs.setTotalExperience(job.getTotalExperience());
			jobs.setDegree(job.getDegree());
			jobs.setJobduration(job.getJobduration());
			jobs.setTerm(job.getTerm());

			employer.getJobs().add(jobs);
			jobs.setEmployer(employer);
			getSession().saveOrUpdate(jobs);
			commit();
			return jobs;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating employer: " + e.getMessage());
		}
	}

	public Job viewJobDetail(int jobId) throws Exception {
		System.out.println("DAO me job Id is :" + jobId);
		try {
			begin();
			TypedQuery<Job> q = getSession().createQuery("From Job where jobId=:jobId", Job.class);
			q.setParameter("jobId", jobId);
			Job job = new Job();
			job = (Job) q.getSingleResult();
			commit();
			return job;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get job " + jobId, e);
		}
	}

	public void deleteJob(int jobId) throws Exception {
		System.out.println("DAO me job Id is :" + jobId);
		try {
			begin();
			System.out.println("deleted job application");
			Query q2 = getSession().createQuery("Delete From Job where jobId=:jobId");
			q2.setParameter("jobId", jobId);
			q2.executeUpdate();
			commit();
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get job " + jobId, e);
		}
	}

	public List<Job> getJobList() throws Exception {
		try {
			begin();
			System.out.println("Inside get job List DAO");
			TypedQuery<Job> q2 = getSession().createQuery("From Job", Job.class);
			List<Job> listJob = q2.getResultList();
			commit();
			return listJob;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating student: " + e.getMessage());
		}
	}

	public List<Job> viewJobsByFilters(String inputJobTitle, String jobType) throws Exception {
		// TODO Auto-generated method stub

		try {
			begin();
			System.out.println("Inside get job List Filters DAO");
			Criteria c = getSession().createCriteria(Job.class);
			c.add(Restrictions.ilike("jobTitle", inputJobTitle, MatchMode.ANYWHERE));
			c.add(Restrictions.ilike("jobType", jobType, MatchMode.ANYWHERE));
			List<Job> listJob = c.list();
			commit();
			return listJob;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating student: " + e.getMessage());
		}
	}

	public Job updateJobDetails(String userName, Job job, int jobId) throws Exception {
		try {
			begin();
			System.out.println("Inside update job details DAO");

			System.out.println(userName);
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = (User) query.getSingleResult();
			TypedQuery<Employer> q2 = getSession().createQuery("From Employer where userId=:userId", Employer.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());
			Employer employer = (Employer) q2.getSingleResult();

			Job jobs = new Job();
			TypedQuery<Job> q = getSession().createQuery("From Job where jobId=:jobId", Job.class);
			q.setParameter("jobId", jobId);
			jobs = (Job) q.getSingleResult();

			jobs.setJobTitle(job.getJobTitle());
			jobs.setJobDesc(job.getJobDesc());
			jobs.setJobType(job.getJobType());
			jobs.setJobduration(job.getJobduration());
			jobs.setJobLocation(job.getJobLocation());
			jobs.setSalary(job.getSalary());
			jobs.setTerm(job.getTerm());
			employer.getJobs().add(jobs);
			jobs.setEmployer(employer);
			getSession().update(jobs);
			commit();
			return jobs;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating employer: " + e.getMessage());
		}
	}
}
