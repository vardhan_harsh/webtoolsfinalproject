package com.northeastern.nucareers.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;

import com.northeastern.nucareers.pojo.Job;
import com.northeastern.nucareers.pojo.JobApplication;
import com.northeastern.nucareers.pojo.Student;
import com.northeastern.nucareers.pojo.User;

public class JobApplicationDAO extends DAO {

	public JobApplicationDAO() {
	}

	public List<JobApplication> getEmpAppList(String userName) throws Exception {

		try {
			begin();
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = new User();
			userAccount = (User) query.getSingleResult();
			Query<JobApplication> q = getSession().createQuery(
					"From JobApplication where job_id IN (select jobId from Job where employer_id= :x)",
					JobApplication.class);
			q.setParameter("x", userAccount.getUserId());
			List<JobApplication> jobApplication = new ArrayList<>();
			jobApplication = q.getResultList();
			commit();
			return jobApplication;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get student " + userName, e);
		}
	}

	public JobApplication getStudentJobApplication(String userName, int jobId) throws Exception {
		JobApplication jobApplication = new JobApplication();
		try {
			begin();
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = new User();
			userAccount = (User) query.getSingleResult();
			long x = userAccount.getUserId();
			System.out.println("+++++++++++" + x);
			System.out.println("+++++++++++" + jobId);
			try {
				Query<JobApplication> q = getSession().createQuery(
						"from JobApplication where job_id= :jobid and student_id= :studentid", JobApplication.class);
				q.setParameter("jobid", jobId);
				q.setParameter("studentid", x);
				q.setMaxResults(1);
				jobApplication = (JobApplication) q.uniqueResult();
				System.out.println("Student Message is: " + jobApplication.getMessage());
			} catch (NullPointerException e) {
				System.out.print("NullPointerException caught");
				jobApplication = null;
				commit();
				return jobApplication;
			} catch (NoResultException e) {
				System.out.print("NoResultException caught");
				jobApplication = null;
				commit();
				return jobApplication;
			}
			commit();
		} catch (HibernateException e) {
			rollback();
			System.out.println("Hibernate error while searching job application" + e.getMessage());
		}
		return jobApplication;
	}

	public JobApplication createJobApplication(String userName, int jobId, String message) throws Exception {

		JobApplication application = new JobApplication();
		try {
			begin();
			System.out.println("Inside Emp DAO");

			System.out.println(userName);
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = new User();
			userAccount = (User) query.getSingleResult();
			TypedQuery<Student> q2 = getSession().createQuery("From Student where userId=:userId", Student.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());

			Student student = new Student();
			student = (Student) q2.getSingleResult();
			TypedQuery<Job> q3 = getSession().createQuery("From Job where jobId=:jobId", Job.class);
			q3.setParameter("jobId", jobId);

			Job job = new Job();
			job = (Job) q3.getSingleResult();
			JobApplication jobapp = new JobApplication();
			jobapp.setMessage(message);
			jobapp.setDateOfApplication(new Date());
			jobapp.setStatus("Applied");
			student.getJobApplication().add(jobapp);
			job.getJobApplication().add(jobapp);
			jobapp.setStudent(student);
			jobapp.setJob(job);
			getSession().saveOrUpdate(jobapp);
			commit();
			return application;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating application: " + e.getMessage());
		}
	}

	public List<JobApplication> getStudentJobApplicationList(String userName) throws Exception {

		try {
			begin();
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = new User();
			userAccount = (User) query.getSingleResult();
			TypedQuery<JobApplication> q = getSession().createQuery(
					"From JobApplication where student_id=:studentid order by dateOfApplication desc",
					JobApplication.class);
			q.setParameter("studentid", userAccount.getUserId());
			List<JobApplication> jobApplication = new ArrayList<>();
			jobApplication = q.getResultList();
			commit();
			return jobApplication;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not find job application " + userName, e);
		}
	}

	public JobApplication setDecision(int applicationId, String empmessage, String status) throws Exception {

		try {
			begin();
			System.out.println("Making Decision");
			TypedQuery<JobApplication> q = getSession()
					.createQuery("From JobApplication where jobApplicationId=:applicationId", JobApplication.class);
			q.setParameter("applicationId", applicationId);
			JobApplication jobApplication = new JobApplication();
			jobApplication = (JobApplication) q.getSingleResult();
			jobApplication.setEmpmessage(empmessage);
			jobApplication.setStatus(status);

			if (status.equalsIgnoreCase("Revoke")) {
				jobApplication.getStudent().setJobTitle(null);
				jobApplication.getStudent().setCompany(null);
				jobApplication.getStudent().setIsAvailable("yes");
			}

			System.out.println("Done");
			getSession().saveOrUpdate(jobApplication);

			commit();
			return jobApplication;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not find job application " + e.getMessage());
		}
	}

	public List<JobApplication> jobApplicationsOffered(String userName) throws Exception {
		try {
			begin();
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = new User();
			userAccount = (User) query.getSingleResult();
			TypedQuery<JobApplication> q = getSession().createQuery(
					"From JobApplication where student_id=:studentid and status = :stat order by dateOfApplication desc",
					JobApplication.class);
			q.setParameter("studentid", userAccount.getUserId());
			q.setParameter("stat", "Job Offered");
			List<JobApplication> jobApplication = new ArrayList<>();
			jobApplication = q.getResultList();
			commit();
			for (JobApplication ja : jobApplication) {
				System.out.println("STUDENT AVAILABILITY " + ja.getStudent().getIsAvailable());
			}
			return jobApplication;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not find job application " + userName, e);
		}
	}

	public JobApplication setJobofferDecision(String userName, int jobApplicationId, String decision) throws Exception {
		try {
			begin();
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = new User();
			userAccount = (User) query.getSingleResult();
			TypedQuery<JobApplication> q = getSession().createQuery(
					"From JobApplication where student_id=:studentid and jobApplicationId = :appId",
					JobApplication.class);
			q.setParameter("studentid", userAccount.getUserId());
			q.setParameter("appId", jobApplicationId);
			JobApplication jobApplication = new JobApplication();
			jobApplication = q.getSingleResult();
			jobApplication.getStudent().setJobTitle(jobApplication.getJob().getJobTitle());
			jobApplication.getStudent()
					.setCompany(jobApplication.getJob().getEmployer().getEmpOrg().getOrganizationName());
			if (decision.equalsIgnoreCase("reject"))
				jobApplication.getStudent().setIsAvailable("yes");
			else if (decision.equalsIgnoreCase("accept"))
				jobApplication.getStudent().setIsAvailable("no");
			jobApplication.setAction(decision);
			getSession().saveOrUpdate(jobApplication);
			commit();
			return jobApplication;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not find job application " + userName, e);
		}
	}
}
