package com.northeastern.nucareers.dao;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class EmailDAO {

	public void sendEmail(String useremail, String message) {
		try {
			Email email = new SimpleEmail();
			email.setHostName("smtp.googlemail.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator("contactapplication2018@gmail.com", "springmvc"));
			email.setSSLOnConnect(true);
			email.setFrom("no-reply@msis.neu.edu"); // This user email does not
													// exist
			email.setSubject("Password Reminder");
			email.setMsg(message); // Retrieve email from the DAO and send this
			email.addTo(useremail);
			email.send();
		} catch (EmailException e) {
			System.out.println("Email cannot be sent");
		}
	}

	public void sendResponseMail(String senderEmail, String empFullName, String company, String applicantName,
			String applicantEmail, String empmessage, String status, String jobTitle) {
		// TODO Auto-generated method stub
		try {
			Email email = new SimpleEmail();
			email.setHostName("smtp.googlemail.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator("contactapplication2018@gmail.com", "springmvc"));
			email.setSSLOnConnect(true);
			email.setFrom(senderEmail); // This user email does not exist
			String htmlResponse = "<p>Hi " + applicantName + ",</p>\r\n" + "<p>" + empmessage + "</p><p>Regards</p><p>"
					+ empFullName + "</p><p><b>" + company + "</b></p>";
			email.setContent(htmlResponse, "text/html; charset=utf-8");
			email.setSubject(jobTitle + " - " + status);
			email.addTo(applicantEmail);
			email.send();
		} catch (EmailException e) {
			System.out.println("Email cannot be sent");
		}
	}
}
