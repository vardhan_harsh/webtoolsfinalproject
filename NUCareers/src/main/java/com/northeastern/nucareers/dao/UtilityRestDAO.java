package com.northeastern.nucareers.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.northeastern.nucareers.pojo.Country;
import com.northeastern.nucareers.pojo.Result;
import com.northeastern.nucareers.pojo.States;

public class UtilityRestDAO {

	RestTemplate restTemplate;
	List<HttpMessageConverter<?>> messageConverters;
	MappingJackson2HttpMessageConverter converter;
	Country country;

	public UtilityRestDAO() {
		// TODO Auto-generated constructor stub
		restTemplate = new RestTemplate();
		messageConverters = new ArrayList<HttpMessageConverter<?>>();
		converter = new MappingJackson2HttpMessageConverter();
		// Note: here we are making this converter to process any kind of response,
		// not only application/*json, which is the default behaviour
		converter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
		messageConverters.add(converter);
		restTemplate.setMessageConverters(messageConverters);
		// Reference website - https://www.printful.com/docs/countries
		country = restTemplate.getForObject("https://api.printful.com/countries", Country.class);
	}

	public List<String> getCountryList() {
		List<String> countriesNameList = new ArrayList<String>();
		for (Result r : country.getResult()) {
			countriesNameList.add(r.getName());
		}
		return countriesNameList;
	}

	public List<String> getStateListByCountry(String code) {
		List<String> stateList = new ArrayList<String>();
		for (Result r : country.getResult()) {
			if (r.getCode().equalsIgnoreCase(code)) {
				for (States s : r.getStates()) {
					stateList.add(s.getCode());
				}
				break;
			}
		}
		return stateList;
	}

}
