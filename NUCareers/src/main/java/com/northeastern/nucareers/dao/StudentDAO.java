package com.northeastern.nucareers.dao;

import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;

import com.northeastern.nucareers.pojo.Student;
import com.northeastern.nucareers.pojo.User;

public class StudentDAO extends DAO {

	public Student viewProfile(String userName) throws Exception {
		try {
			begin();
			System.out.println("Inside profile DAO");

			System.out.println(userName);
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = (User) query.getSingleResult();
			TypedQuery<Student> q2 = getSession().createQuery("From Student where userId=:userId", Student.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());
			Student student = (Student) q2.getSingleResult();
			commit();
			return student;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating student: " + e.getMessage());
		}
	}

	public Student updateStudentProfile(Student student) throws Exception {
		try {
			begin();
			System.out.println("Inside student DAO");

			System.out.println(student.getUserName());
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", student.getUserName());
			User userAccount = (User) query.getSingleResult();
			TypedQuery<Student> q2 = getSession().createQuery("From Student where userId=:userId", Student.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());
			Student std = (Student) q2.getSingleResult();

			std.setFirstName(student.getFirstName());
			std.setEmail(student.getEmail());
			std.setLastName(student.getLastName());
			std.setPassword(student.getPassword());
			std.setUserName(student.getUserName());
			std.getAddress().setAddressLine1(student.getAddress().getAddressLine1());
			std.getAddress().setAddressLine2(student.getAddress().getAddressLine2());
			std.getAddress().setAddressLine3(student.getAddress().getAddressLine3());
			std.getAddress().setCity(student.getAddress().getCity());
			std.getAddress().setCountry(student.getAddress().getCountry());
			std.getAddress().setState(student.getAddress().getState());
			std.getAddress().setZipCode(student.getAddress().getZipCode());
			std.setPhoneNumber(student.getPhoneNumber());
			getSession().update(std);
			commit();
			return std;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating student: " + e.getMessage());
		}
	}

	public Student uploadResume(String userName, String path, String name) throws Exception {
		try {
			begin();
			System.out.println("Inside Upload Resume");
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = (User) query.getSingleResult();
			TypedQuery<Student> q2 = getSession().createQuery("From Student where userId=:userId", Student.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());
			Student student = (Student) q2.getSingleResult();
			student.setFileName(name);
			student.setFilePath(path);
			getSession().update(student);
			commit();
			return student;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating student: " + e.getMessage());
		}

	}

	public Student submitPortfolio(String userName, String degree, String major, float gpa, String preferedIndustry,
			String workExperience) throws Exception {
		try {
			begin();
			System.out.println("Inside submit portfolio DAO");

			System.out.println(userName);
			TypedQuery<User> query = getSession().createQuery("From User where userName=:userName", User.class);
			query.setParameter("userName", userName);
			User userAccount = (User) query.getSingleResult();
			TypedQuery<Student> q2 = getSession().createQuery("From Student where userId=:userId", Student.class);
			System.out.println("***********" + userAccount.getUserId());
			q2.setParameter("userId", userAccount.getUserId());
			Student student = (Student) q2.getSingleResult();

			student.setDegree(degree);
			student.setMajor(major);
			student.setGpa(gpa);
			student.setPreferedIndustry(preferedIndustry);
			student.setWorkExperience(workExperience);
			getSession().update(student);
			commit();
			return student;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating student: " + e.getMessage());
		}

	}
}
