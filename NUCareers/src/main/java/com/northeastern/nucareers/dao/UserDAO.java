package com.northeastern.nucareers.dao;

import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;

import com.northeastern.nucareers.pojo.Address;
import com.northeastern.nucareers.pojo.Employer;
import com.northeastern.nucareers.pojo.EmployerDivision;
import com.northeastern.nucareers.pojo.EmployerOrganization;
import com.northeastern.nucareers.pojo.Student;
import com.northeastern.nucareers.pojo.User;

public class UserDAO extends DAO {
	public UserDAO() {
	}

	public User get(String username, String password) throws Exception {
		try {
			begin();
			Query<User> q = getSession().createQuery("from User where userName = :username and password = :password",
					User.class);
			q.setParameter("username", username);
			q.setParameter("password", password);
			User user = (User) q.uniqueResult();
			commit();
			return user;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get user " + username, e);
		}
	}

	public User getByUsername(String userName) {
		try {
			begin();
			Query<User> q = getSession().createQuery("from User where userName = :username", User.class);
			q.setParameter("username", userName);
			User user = (User) q.uniqueResult();
			commit();
			return user;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;

	}

	public User get(String userEmail) {
		try {
			begin();
			TypedQuery<User> q = getSession().createQuery("from User where email = :useremail", User.class);
			q.setParameter("useremail", userEmail);
			User user = (User) q.getSingleResult();
			commit();
			return user;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;

	}

	public boolean updateUser(String email) throws Exception {
		try {
			begin();
			System.out.println("inside DAO");
			TypedQuery<User> q = getSession().createQuery("from User where email = :useremail", User.class);
			q.setParameter("useremail", email);
			User user = (User) q.getSingleResult();
			if (user != null) {
				user.setStatus(1);
				getSession().update(user);
				commit();
				return true;
			} else {
				commit();
				return false;
			}

		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating user: " + e.getMessage());
		}

	}

	public User registerEmployer(Employer emp) throws Exception {
		try {
			begin();
			System.out.println("inside DAO");
			Address address = new Address(emp.getAddress().getAddressLine1(), emp.getAddress().getAddressLine2(),
					emp.getAddress().getAddressLine3(), emp.getAddress().getCity(), emp.getAddress().getZipCode(),
					emp.getAddress().getState(), emp.getAddress().getCountry());

			EmployerOrganization empOrg = new EmployerOrganization(emp.getEmpOrg().getOrganizationName(),
					emp.getEmpOrg().getIndustry(), emp.getEmpOrg().getWebsite(), emp.getEmpOrg().getDescription());

			EmployerDivision empDiv = new EmployerDivision(emp.getEmpDiv().getDivisionName(),
					emp.getEmpDiv().getIndustry(), emp.getEmpDiv().getWebsite(), emp.getEmpDiv().getDescription());

			Employer employer = new Employer(emp.getFirstName(), emp.getLastName(), emp.getContactCollege(),
					emp.getTitle(), emp.getPhoneNumber(), emp.getNuAlumnus());

			employer.setAddress(address);
			employer.setEmpDiv(empDiv);
			employer.setEmpOrg(empOrg);
			employer.setUserId(emp.getUserId());
			employer.setEmail(emp.getEmail());
			employer.setUserName(emp.getUserName());
			employer.setPassword(emp.getPassword());
			employer.setStatus(emp.getStatus());
			employer.setUserType(emp.getUserType());
			empOrg.setEmployer(employer);
			address.setUser(employer);
			empDiv.setEmployer(employer);
			getSession().save(employer);
			commit();
			return employer;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating Employer: " + e.getMessage());
		}
	}

	public User registerStudent(Student student) throws Exception {
		try {
			begin();
			Student std = new Student();
			Address address = new Address(student.getAddress().getAddressLine1(),
					student.getAddress().getAddressLine2(), student.getAddress().getAddressLine3(),
					student.getAddress().getCity(), student.getAddress().getZipCode(), student.getAddress().getState(),
					student.getAddress().getCountry());

			Query<Student> q = getSession().createQuery("select max(nuid) from Student", Student.class);
			long nuid = 1000000;
			if (!q.list().isEmpty()) {
				nuid = q.list().get(0).getNuid() + 1;
			}
			std.setNuid(nuid);
			std.setFirstName(student.getFirstName());
			std.setEmail(student.getEmail());
			std.setGender(student.getGender());
			std.setLastName(student.getLastName());
			std.setPassword(student.getPassword());
			std.setStatus(student.getStatus());
			std.setUserId(student.getUserId());
			std.setUserName(student.getUserName());
			std.setUserType(student.getUserType());
			std.setVisaType(student.getVisaType());
			std.setGpa(student.getGpa());
			std.setDegree(student.getDegree());
			std.setMajor(student.getMajor());
			std.setAddress(address);
			std.setPhoneNumber(student.getPhoneNumber());
			address.setUser(std);
			getSession().save(std);
			commit();
			return std;
		} catch (Exception e) {
			rollback();
			throw new Exception("Exception while creating Employer: " + e.getMessage());
		}
	}
}
