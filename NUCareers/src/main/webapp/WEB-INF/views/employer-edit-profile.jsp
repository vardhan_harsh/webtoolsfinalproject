<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.captcha.botdetect.web.servlet.Captcha"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="employer-nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<c:if test="${errorMessage !=null}">
				<div class="alert alert-danger fade in" style="text-align: center;">
					<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
					${errorMessage}
				</div>
			</c:if>
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<form:form class="panel-group" id="accordion"
					modelAttribute="employer"
					action="${contextPath}/employer/updateprofile.htm" method="post">
					<h2>Edit Profile</h2>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapseOne">Organization Information</a>
							</h3>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="form-horizontal">
									<div class="form-group">
										<label for="inputName" class="control-label col-xs-2">Organization
											Name</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputName"
												id="inputOrgName" path="empOrg.organizationName"
												readonly="true" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputIndustry" class="control-label col-xs-2">Industry</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputIndustry"
												id="inputOrgIndustry" path="empOrg.industry" readonly="true"
												required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputWebsite" class="control-label col-xs-2">Website</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputWebsite"
												id="inputOrgWebsite" path="empOrg.website"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputDescription" class="control-label col-xs-2">Description</label>
										<div class="col-xs-7">
											<form:textarea cssClass="form-control"
												name="inputDescription" id="inputOrgDescription"
												path="empOrg.description"></form:textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapseTwo">Division Information</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="form-horizontal">
									<div class="form-group">
										<label for="inputDivName" class="control-label col-xs-2">Division
											Name</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputDivName"
												id="inputDivName" path="empDiv.divisionName" readonly="true"
												required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputDivIndustry" class="control-label col-xs-2">Industry</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputDivIndustry"
												id="inputDivIndustry" path="empDiv.industry" readonly="true"
												required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputDivWebsite" class="control-label col-xs-2">Website</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputDivWebsite"
												id="inputDivWebsite" path="empDiv.website"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputDivDescription"
											class="control-label col-xs-2">Description</label>
										<div class="col-xs-7">
											<form:textarea class="form-control"
												name="inputDivDescription" id="inputDivDescription"
												path="empDiv.description"></form:textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Personal Details -->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapseThree">User Information</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="form-horizontal">
									<div class="form-group">
										<label class="control-label col-xs-2"
											style="text-decoration: underline; display: block; font-size: 1em; margin-top: 1.33em; margin-bottom: 1.33em; margin-left: 0; margin-right: 0; font-weight: bold;">Login
											Information</label>
									</div>
									<div class="form-group">
										<label class="control-label col-xs-2">Email Address </label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputEmail"
												path="email" readonly="true" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPassword" class="control-label col-xs-2">Password</label>
										<div class="col-xs-7">
											<form:password cssClass="form-control" name="inputPassword"
												path="password" required="true"></form:password>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-xs-2">NU Primary
											Contact College or Department</label>
										<div class="col-xs-7">
											<form:label cssClass="form-control-static"
												path="contactCollege">College of Engineering</form:label>
										</div>
									</div>
									<div class="form-group">
										<label for="inputfirstname" class="control-label col-xs-2">First
											Name</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputfirstname"
												path="firstName" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputfirstname" class="control-label col-xs-2">Last
											Name</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputlastname"
												path="lastName" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputtitle" class="control-label col-xs-2">Title</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputtitle"
												path="title" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputaddress1" class="control-label col-xs-2">Address
											1</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputaddress1"
												path="address.addressLine1" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputaddress2" class="control-label col-xs-2">Address
											2</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputaddress2"
												path="address.addressLine2"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputaddress3" class="control-label col-xs-2">Address
											3</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputaddress3"
												path="address.addressLine3"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputcity" class="control-label col-xs-2">City</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="city"
												path="address.city" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputProvince" class="control-label col-xs-2">Province
											/ State</label>
										<div class="col-xs-7">
											<form:select path="address.state" cssClass="form-control"
												required="true">
												<form:option value="" label="--- Select ---" />
												<form:option value="Not Applicable" label="Not Applicable" />
												<form:options items="${stateList}" />
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPostal" class="control-label col-xs-2">Postal
											Code / Zip Code</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputPostal"
												path="address.zipCode" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputCountry" class="control-label col-xs-2">Country</label>
										<div class="col-xs-7">
											<form:select path="address.country" cssClass="form-control"
												required="true">
												<form:option value="" label="--- Select ---" />
												<form:options items="${countryList}" />
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPhone" class="control-label col-xs-2">Phone
											Number</label>
										<div class="col-xs-7">
											<form:input cssClass="form-control" name="inputPhone"
												path="phoneNumber" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputAlumnus" class="control-label col-xs-2">NU
											Alumnus</label>
										<div class="col-xs-7">
											<form:select path="nuAlumnus" cssClass="form-control"
												required="true">
												<form:option value="" label="--- Select ---" />
												<form:option value="Yes" label="Yes" />
												<form:option value="No" label="No" />
											</form:select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary m-t-20">Update
						Profile</button>
				</form:form>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>