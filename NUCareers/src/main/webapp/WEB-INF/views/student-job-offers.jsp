<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"> --%>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="student-nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<c:if test="${message !=null}">
					<div class="alert alert-success fade in"
						style="text-align: center;">
						<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong>
						${message}
					</div>
				</c:if>
				<h2>Applicant's Applications</h2>
				<hr>

				<table class="table table-striped">
					<thead>
						<tr>
							<th>Company Name</th>
							<th>Applied Job</th>
							<th>Applied Date</th>
							<th>Employer's Message</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${appList.size() == 0}">
								<tr>
									<td colspan="5">No jobs offer yet</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="application" items="${appList}">
									<tr>
										<td>${application.job.employer.empOrg.organizationName}</td>
										<td>${application.job.jobTitle}</td>
										<td>${application.dateOfApplication}</td>
										<td>${application.empmessage}</td>
										<td>${application.status}</td>
										<c:choose>
											<c:when test="${application.student.isAvailable == 'yes' }">
												<td><form
														action="${contextPath}/student/studentdecision.htm">
														<label class="radio-inline"><input type="radio"
															name="decision" required="required" value="accept">
															Accept</label> <label class="radio-inline"><input
															type="radio" name="decision" value="reject">
															Reject</label> <input type="hidden" name="jobapp"
															value="${application.jobApplicationId }" />
														<button type="submit" class="btn btn-primary"
															style="display: inline; margin-left: 10px; padding-top: 0px; padding-bottom: 0px">Submit</button>
													</form></td>
											</c:when>
											<c:otherwise>
												<c:set var="columnText"
													value="${fn:toUpperCase(application.action)}ED" />
												<td>${application.action != null? columnText : 'NA'}</td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>