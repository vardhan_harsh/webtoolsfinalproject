<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!-- 	<nav class="navbar navbar-light bg-light fixed-top"> <a -->
	<!-- 		class="navbar-brand" href="#"> <img -->
	<%-- 		src="<c:url value="/resources/images/NUcareersLogo.png"/>" alt=""> --%>
	<!-- 	</a> </nav> -->
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<nav class="navbar navbar-default navbar-fixed-top"
		style="padding-bottom: 35px">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<a href="${contextPath}/login/login.htm" class="navbar-brand"> <img
				src="<c:url value="/resources/images/NUcareersLogo.png"/>" alt=""></a>
		</div>
	</nav>
</body>
</html>