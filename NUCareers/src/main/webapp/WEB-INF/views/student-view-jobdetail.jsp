<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"> --%>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->

<style>
ol.progtrckr {
	margin: 0;
	padding: 0;
	list-style-type
	none;
}

ol.progtrckr li {
	display: inline-block;
	text-align: center;
	line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li {
	width: 49%;
}

ol.progtrckr[data-progtrckr-steps="3"] li {
	width: 33%;
}

ol.progtrckr[data-progtrckr-steps="4"] li {
	width: 24%;
}

ol.progtrckr[data-progtrckr-steps="5"] li {
	width: 19%;
}

ol.progtrckr[data-progtrckr-steps="6"] li {
	width: 16%;
}

ol.progtrckr[data-progtrckr-steps="7"] li {
	width: 14%;
}

ol.progtrckr[data-progtrckr-steps="8"] li {
	width: 12%;
}

ol.progtrckr[data-progtrckr-steps="9"] li {
	width: 11%;
}

ol.progtrckr li.progtrckr-done {
	color: black;
	border-bottom: 4px solid yellowgreen;
}

ol.progtrckr li.progtrckr-todo {
	color: silver;
	border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
	content: "\00a0\00a0";
}

ol.progtrckr li:before {
	position: relative;
	bottom: -2.5em;
	float: left;
	left: 50%;
	line-height: 1em;
}

ol.progtrckr li.progtrckr-done:before {
	content: "\2713";
	color: white;
	background-color: yellowgreen;
	height: 2.2em;
	width: 2.2em;
	line-height: 2.2em;
	border: none;
	border-radius: 2.2em;
}

ol.progtrckr li.progtrckr-todo:before {
	content: "\039F";
	color: silver;
	background-color: white;
	font-size: 2.2em;
	bottom: -1.2em;
}
</style>

</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="student-nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<c:if test="${errorMessage !=null}">
				<div class="alert alert-danger fade in" style="text-align: center;">
					<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!
					</strong> ${errorMessage}
				</div>
			</c:if>
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<h2>Job Details</h2>
				<c:if test="${jobapp  !=null && jobapp.status != 'Revoke'}">
					<hr>
					<h1 id="msg">
						Applied <span class="glyphicon glyphicon-ok" style="color: green"></span>
					</h1>
					<input id="status" type="hidden" value="${ jobapp.status}" />
					<ol id="jobStatusList" class="progtrckr" data-progtrckr-steps="4">
						<li class="progtrckr-todo" value="Applied">Application
							Submitted</li>

						<li class="progtrckr-todo" value="Under Review">Under Review</li>

						<li class="progtrckr-todo" value="Interview Scheduled">Interview
							Scheduled</li>

						<li class="progtrckr-todo" value="Job Offered">Job Offered</li>
					</ol>
				</c:if>
				<hr>
				<table class="table table-striped">
					<tbody>

						<tr>
							<th scope="row" class="col-md-2">Company Name</th>
							<td>${job.employer.empOrg.organizationName}</td>
						</tr>

						<tr>
							<th scope="row" class="col-md-2">Job Title</th>
							<td>${job.jobTitle}</td>
						</tr>

						<tr>
							<th scope="row" class="col-md-2">Job Description</th>
							<td>${job.jobDesc}</td>
						</tr>

						<tr>
							<th scope="row" class="col-md-2">Job Location</th>
							<td>${job.jobLocation}</td>
						</tr>

						<tr>
							<th scope="row" class="col-md-2">Salary</th>
							<td>${job.salary}</td>
						</tr>

						<tr>
							<th scope="row" class="col-md-2">Job Duration</th>
							<td>${job.jobduration}</td>
						</tr>

						<tr>
							<th scope="row" class="col-md-2">Job Term</th>
							<td>${job.term}</td>
						</tr>
					</tbody>
				</table>
				<form id="jobdetail" action="${contextPath }/student/applyjob.htm"
					class="form-horizontal" method="post">
					<div class="form-group">
						<div class="col-xs-12" style="text-align: right;">
							<input type="hidden" id="jobId" name="jobId" value="${job.jobId}" />
							<a href="${contextPath}/student/viewjobs.htm"
								class="btn btn-primary" style="float: left;">Back</a>

							<button type="submit" class="btn btn-primary"
								<c:if test="${studentCompany != null || (jobapp.status != null && jobapp.status != 'Revoke' )}">disabled</c:if>>Apply</button>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
	<script>
		var status = $("#status").val();
		$("#msg").find("#decision").remove();
		if (status == "Rejected") {
			$("#msg")
					.append(
							"<span id='decision' style='float: right; display: inline;'>No longer under consideration</span>");
		} else {
			var listItems = $("#jobStatusList li");
			listItems.each(function(idx, li) {
				var stat = $(li);
				stat.removeClass('progtrckr-todo');
				stat.addClass('progtrckr-done');
				if (stat.attr("value") === status) {
					return false;
				}
			});
		}
	</script>
</body>
</html>