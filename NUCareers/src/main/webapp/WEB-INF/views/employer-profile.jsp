<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"> --%>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="employer-nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<table class="table table-striped">
					<tbody>
						<tr>
							<th scope="row"><h1>General Information</h1></th>
							<th><a href="${contextPath}/employer/editprofile.htm"> <span
									class="glyphicon glyphicon-pencil"
									style="margin-top: 12px; float: right; padding-right: 10px"></span>
							</a></th>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">Company Name</th>
							<td class="col-md-2">${employer.empOrg.organizationName}</td>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">First Name</th>
							<td class="col-md-2">${employer.firstName}</td>
						</tr>
						<tr>
							<th scope="row">Last Name</th>
							<td>${employer.lastName }</td>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">Contact Number</th>
							<td class="col-md-2">${employer.phoneNumber}</td>
						</tr>
						<tr>
							<th scope="row">Email</th>
							<td>${employer.email}</td>
						</tr>
					</tbody>
				</table>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>