<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page import="com.captcha.botdetect.web.servlet.Captcha"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<c:if test="${message !=null}">
					<div class="alert alert-success fade in"
						style="text-align: center;">
						<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong>
						${message}
					</div>
				</c:if>
				<form:form cssClass="form-horizontal" modelAttribute="student"
					action="${contextPath}/login/createStudent.htm" method="post">
					<div class="form-group">
						<h2 class="control-label col-xs-3" style="text-align: left;">Student
							Registration</h2>
					</div>
					<hr>
					<div class="form-group">
						<label class="control-label col-xs-2">Username</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputUserName"
								path="userName" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword" class="control-label col-xs-2">Password</label>
						<div class="col-xs-7">
							<form:password cssClass="form-control" name="inputpassword"
								path="password" required="required"></form:password>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-2">Email Address </label>
						<div class="col-xs-7">
							<form:input type="email" cssClass="form-control"
								name="inputEmail" path="email" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputfirstname" class="control-label col-xs-2">First
							Name</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputfirstname"
								path="firstName" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputfirstname" class="control-label col-xs-2">Last
							Name</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputlastname"
								path="lastName" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-2">Gender</label>
						<div class="col-xs-7">
							<form:select path="gender" cssClass="form-control"
								required="required">
								<form:option value="" label="--- Select ---" />
								<form:option value="Male" label="Male" />
								<form:option value="Female" label="Female" />
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputfirstname" class="control-label col-xs-2">Visa
							Type</label>
						<div class="col-xs-7">
							<form:select path="visaType" cssClass="form-control"
								required="required">
								<form:option value="" label="--- Select ---" />
								<form:option value="F1" label="F1" />
								<form:option value="J1" label="J1" />
								<form:option value="M1" label="M1" />
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputgpa" class="control-label col-xs-2">GPA</label>
						<div class="col-xs-7">
							<form:input type="number" id="gpa" cssClass="form-control"
								name="inputjobLocation" path="gpa" required="required"></form:input>
						</div>
						<label id="inputOut" class="control-label col-xs-2"
							style="text-align: left;">Out of 4.0</label>
					</div>
					<div class="form-group">
						<label for="inputdegree" class="control-label col-xs-2">Academic
							Degree</label>
						<div class="col-xs-7">
							<div class="radio">
								<label><form:radiobutton path="degree"
										value="Bacherlor's Degree" label="Bacherlor's Degree"
										required="required"></form:radiobutton></label>
							</div>
							<div class="radio">
								<label><form:radiobutton path="degree"
										value="Master's Degree" label="Master's Degree"></form:radiobutton></label>
							</div>
							<div class="radio">
								<label><form:radiobutton path="degree"
										value="Doctorate Degree" label="Doctorate Degree"></form:radiobutton></label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputMajor" class="control-label col-xs-2">Major</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputMajor"
								path="major" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputaddress1" class="control-label col-xs-2">Address
							1</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputaddress1"
								path="address.addressLine1" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputaddress2" class="control-label col-xs-2">Address
							2</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputaddress2"
								path="address.addressLine2"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputaddress3" class="control-label col-xs-2">Address
							3</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputaddress3"
								path="address.addressLine3"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputcity" class="control-label col-xs-2">City</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="city"
								path="address.city" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputProvince" class="control-label col-xs-2">Province
							/ State</label>
						<div class="col-xs-7">
							<form:select path="address.state" cssClass="form-control"
								required="required">
								<form:option value="" label="--- Select ---" />
								<form:option value="Not Applicable" label="Not Applicable" />
								<form:options items="${stateList}" />
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPostal" class="control-label col-xs-2">Postal
							Code / Zip Code</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputPostal"
								path="address.zipCode" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputCountry" class="control-label col-xs-2">Country</label>
						<div class="col-xs-7">
							<form:select path="address.country" cssClass="form-control"
								required="required">
								<form:option value="" label="--- Select ---" />
								<form:options items="${countryList}" />
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPhone" class="control-label col-xs-2">Phone
							Number</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputPhone"
								path="phoneNumber" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">

						<div class="col-xs-offset-2 col-xs-7">
							<label for="captchaCode" class="prompt">Retype the
								characters from the picture:</label>
							<%
								// Adding BotDetect Captcha to the page
									Captcha captcha = Captcha.load(request, "CaptchaObject");
									captcha.setUserInputID("captchaCode");

									String captchaHtml = captcha.getHtml();
									out.write(captchaHtml);
							%>

							<div class="row">
								<div class="col-xs-3">
									<input class="form-control m-t-10" id="captchaCode" type="text"
										name="captchaCode" required="required" />
								</div>
							</div>
						</div>

					</div>
					<div class="form-group">
				 		<div class="col-xs-offset-2 col-xs-7">
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn btn-primary" onclick="goBack()"
								style="float: right;">Back</button>
						</div>
					</div>
				</form:form>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>

	<script type="text/javascript">
		$('#gpa').keyup(function() {
			if ($(this).val() > 4 || $(this).val() == 0) {
				//alert("No numbers above 100");
				$(this).val('4');
				$(this).popover("show");
			} else {
				$(this).popover("hide");
			}
		});
		$("#gpa").popover({
			title : '<h4>Error</h4>',
			placement : 'top',
			trigger : 'manual',
			content : "<p>GPA cannot be greater than 4 or equal to 0.</p>",
			html : true
		});

		function goBack() {
			window.history.back();
		}
	</script>
</body>
</html>