<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page import="com.captcha.botdetect.web.servlet.Captcha"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"> --%>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="employer-nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<c:if test="${errorMessage !=null}">
				<div class="alert alert-danger fade in" style="text-align: center;">
					<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
					${errorMessage}
				</div>
			</c:if>
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<form:form class="panel-group" id="accordion"
					modelAttribute="faculty"
					action="${contextPath}/login/createFaculty.htm">
					<h2>Faculty Registration</h2>
					<div class="form-group">
						<label class="control-label col-xs-2">Username</label>
						<div class="col-xs-10">
							<form:input cssClass="form-control" name="inputEmail"
								path="userName" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-2">Email Address </label>
						<div class="col-xs-10">
							<form:input cssClass="form-control" name="inputEmail"
								path="email" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword" class="control-label col-xs-2">Password</label>
						<div class="col-xs-10">
							<form:password cssClass="form-control" name="inputPassword"
								path="password" required="required"></form:password>
						</div>
					</div>
					<div class="form-group">
						<label for="inputName" class="control-label col-xs-2">First
							Name</label>
						<div class="col-xs-10">
							<form:input cssClass="form-control" name="inputName"
								id="inputName" path="firstName" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputIndustry" class="control-label col-xs-2">Last
							Name</label>
						<div class="col-xs-10">
							<form:input cssClass="form-control" name="inputLastName"
								id="inputLastName" path="lastName" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputWebsite" class="control-label col-xs-2">Gender</label>
						<div class="col-xs-10">
							<div class="col-xs-7">
								<div class="radio">
									<label><form:radiobutton path="gender" value="Male"
											label="Male"></form:radiobutton></label>
								</div>
								<div class="radio">
									<label><form:radiobutton path="gender" value="Female"
											label="Female"></form:radiobutton></label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-offset-2 col-xs-10">
							<label for="captchaCode" class="prompt">Retype the
								characters from the picture:</label>
							<%
								// Adding BotDetect Captcha to the page
									Captcha captcha = Captcha.load(request, "CaptchaObject");
									captcha.setUserInputID("captchaCode");

									String captchaHtml = captcha.getHtml();
									out.write(captchaHtml);
							%>

							<div class="row">
								<div class="col-xs-3">
									<input class="form-control m-t-10" id="captchaCode" type="text"
										name="captchaCode" required="required" />
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-offset-2 col-xs-10">
							<button type="submit" class="btn btn-primary m-t-20">Submit</button>
						</div>
					</div>
				</form:form>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>