<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/animate/animate.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/css-hamburgers/hamburgers.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/animsition/css/animsition.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/select2/select2.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/daterangepicker/daterangepicker.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->

<style type="text/css">
body {
	background: url(/nucareers/resources/images/welcome.jpeg);
	background-repeat: no-repeat !important;
	background-position: center !important;
	background-size: cover !important;
	background-repeat: no-repeat !important;
}
</style>
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<div class="limiter">
		<c:if test="${message !=null}">
			<div class="alert alert-success fade in" style="text-align: center;">
				<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong>
				${message}
			</div>
		</c:if>
		<div class="container-login100">
			<div class="wrap-login100">

				<form action="<c:url value='j_spring_security_check'/>"
					method="post"
					class="login100-form validate-form p-l-55 p-r-55 p-t-140">
					<span class="login100-form-title"> Northeastern University </span>
					<c:if test="${errorMessage != null}">
						<div class="alert alert-danger fade in"
							style="text-align: center;">
							<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
							${errorMessage}
						</div>
					</c:if>
					<div class="wrap-input100 validate-input m-b-16"
						data-validate="Please enter username">
						<input class="input100" type="text" name="j_username"
							placeholder="Username"> <span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input"
						data-validate="Please enter password">
						<input class="input100" type="password" name="j_password"
							placeholder="Password"> <span class="focus-input100"></span>
					</div>

					<div class="text-right p-t-13 p-b-23">
						<span class="txt1" style="font-size: 11px"> Forgot </span> <a
							href="${contextPath}/login/forgotpassword.htm" class="txt2"
							style="font-size: 11px"> Username / Password? </a>
					</div>

					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn">Sign in</button>
					</div>

					<div class="flex-col-c p-t-40 p-b-40">
						<span class="txt1 p-b-9"> Not registered? </span> <a
							href="${contextPath}/login/register.htm" class="txt3"> Sign
							up now </a>
					</div>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</div>
		</div>
	</div>


	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/animsition/js/animsition.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/vendor/bootstrap/js/popper.js"/>"></script>
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/vendor/select2/select2.min.js"/>"></script>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/daterangepicker/moment.min.js"/>"></script>
	<script
		src="<c:url value="/resources/vendor/daterangepicker/daterangepicker.js"/>"></script>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/countdowntime/countdowntime.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>

</body>
</html>