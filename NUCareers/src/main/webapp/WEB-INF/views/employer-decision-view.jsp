<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"> --%>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="employer-nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<table class="table table-striped">
					<tbody>
						<tr>
							<th scope="row" colspan="2"><h1>General Information</h1></th>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">First Name</th>
							<td class="col-md-2">${student.firstName}</td>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">Last Name</th>
							<td class="col-md-2">${student.lastName }</td>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">Contact Number</th>
							<td class="col-md-2">${student.phoneNumber}</td>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">Email</th>
							<td class="col-md-2">${student.email}</td>
						</tr>
						<tr>
							<td colspan="2"><hr></td>
						</tr>
						<!-- Portfolio -->

						<tr>
							<th scope="row" colspan="2"><h1>Portfolio</h1></th>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">Resume</th>
							<td class="col-md-2"><c:set var="search" value="\\" /> <c:set
									var="myContent" value="${student.filePath}" /> <c:set
									var="replace" value="/" /> <c:set var="myContent"
									value="${fn:replace(myContent, search, replace)}" /> <a
								href="${contextPath}/student/download.htm?name=${myContent}"
								target="_blank">${student.fileName}</a></td>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">Degree</th>
							<td class="col-md-2">${student.degree }</td>

						</tr>
						<tr>
							<th scope="row" class="col-md-2">Major</th>
							<td class="col-md-2">${student.major}</td>

						</tr>
						<tr>
							<th scope="row" class="col-md-2">GPA</th>
							<td class="col-md-2">${student.gpa}</td>

						</tr>

						<!-- Employment History -->
						<tr>
							<td colspan="2"><hr></td>
						</tr>
						<tr>
							<th scope="row" class="col-md-2"><h1>Employment History</h1></th>
							<th></th>
						</tr>

						<tr>
							<th scope="row" class="col-md-2">Employer</th>
							<td class="col-md-2">${student.company}</td>
						</tr>
						<tr>
							<th scope="row" class="col-md-2">Job Title</th>
							<td class="col-md-2">${student.jobTitle }</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<form class="form-horizontal"
					action="${contextPath}/employer/decision.htm" method="post">
					<div class="form-group">
						<div class="col-xs-4">
							<label for="comment">Message</label>
							<textarea class="form-control" rows="5" id="comment"
								name="empmessage" required="required"></textarea>
						</div>
						<div class="col-xs-4">
							<label for="sel1">Decision</label> <select class="form-control"
								id="sel1" name="status" required="required">
								<option value="">--- Select ---</option>
								<c:if test="${appStatus != 'Job Offered'}">
									<option value="Under Review">Under Review</option>
									<option value="Interview Scheduled">Interview
										Scheduled</option>
									<option value="Rejected">Rejected</option>
									<option value="Job Offered">Job Offered</option>
								</c:if>
								<c:if test="${appStatus == 'Job Offered'}">
									<option value="Revoke">Revoke</option>
								</c:if>
							</select>
						</div>
					</div>
					<input type="hidden" id="email" name="email"
						value="${student.email}" /> <input type="hidden" id="firstName"
						name="firstname" value="${student.firstName}" /> <input
						type="hidden" id="applicationId" name="applicationId"
						value="${requestScope.applicationId}" />

					<div class="form-group custom">
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary"
								style="margin-top: 15px;">Submit</button>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>