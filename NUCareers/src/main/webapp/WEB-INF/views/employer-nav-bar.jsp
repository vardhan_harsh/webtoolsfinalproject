<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
a {
	text-decoration: none;
}

.divHi {
	position: relative;
	display: inline;
	float: left;
	padding-top: 15px;
	padding-bottom: 15px; /*! padding-right: 5px; */
	color: #9d9d9d;
	text-decoration: none;
}
</style>


</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<nav class="navbar navbar-inverse"
		style="border-radius: 0px; top: 85px">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" data-target="#navbarCollapse"
				data-toggle="collapse" class="navbar-toggle">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<!-- 			<a href="#" class="navbar-brand">Brand</a> -->
		</div>
		<!-- Collection of nav links and other content for toggling -->
		<div id="navbarCollapse" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="toggle"><a
					href="${contextPath}/employer/dashboard.htm">Home</a></li>
				<li class="toggle"><a
					href="${contextPath}/employer/profile.htm">Profile</a></li>
				<li class="toggle"><a href="${contextPath}/employer/jobs.htm">Job</a></li>
				<li class="toggle"><a
					href="${contextPath}/employer/jobapplications.htm">Job
						Application</a></li>
				<li class="toggle"><a
					href="${contextPath}/employer/postjob.htm">Post Job</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<ins class="divHi">Hi</ins>
				<!-- 				<span>Hi</span> -->
				<li><a href="${contextPath}/employer/profile.htm"
					style="text-decoration: underline;"> ${user.lastName},
						${user.firstName }</a></li>
				<li><a href="${contextPath}/logout.htm">Logout</a></li>
			</ul>
		</div>
	</nav>
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<script type="text/javascript">
		$(document).ready(
				function() {
					// get current URL path and assign 'active' class
					var pathname = window.location.pathname;
					$(".toggle").removeClass("active");
					$('.nav > li > a[href="' + pathname + '"]').parent()
							.addClass('active');
				});
	</script>
</body>
</html>