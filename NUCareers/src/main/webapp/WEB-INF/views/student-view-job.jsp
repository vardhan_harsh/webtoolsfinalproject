<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"> --%>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
<style type="text/css">
.form-inline {
	display: inline;
	float: right;
}
</style>
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="student-nav-bar.jsp" />
	<div class="limiter">

		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">

				<h2>Search Jobs</h2>
				<hr>
				<div class="form-horizontal">
					<div class="form-group">
						<div class="col-xs-12">
							<form action="${contextPath}/student/filterjob.htm" method="post"
								style="float: left; width: 100%;">
								<div class="form-group">
									<label class="control-label col-xs-1" for="inputJobTitle"
										style="width: auto">Job Title</label>
									<div class="col-xs-3">
										<input type="text" class="form-control" name="inputJobTitle"
											placeholder="Job Title">
									</div>
									<label class="control-label col-xs-1" for="inputJotType"
										style="width: auto">Job Type</label>
									<div class="col-xs-3">
										<select name="jobType" class="form-control">
											<option value="" label="--- Select ---">--- Select
												---</option>
											<option value="Intern" label="Intern">Intern</option>
											<option value="Full-Time" label="Full-Time">Full-Time</option>
											<option value="Part-Time" label="Part-Time">Part-Time</option>
										</select>
									</div>
									<div class="col-xs-4">
										<button type="submit" class="btn btn-primary">Search</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div class="col-xs-12">
							<h4 style="margin-bottom: 10px">
								We found ${fn:length(jobList)}
								<c:choose>
									<c:when test="${fn:length(jobList) ==  1}">job </c:when>
									<c:otherwise>jobs </c:otherwise>
								</c:choose>

							</h4>
							<p>${filterResult }</p>
							<c:forEach var="jobs" items="${jobList}">
								<div class="well">
									<div>
										<h3 style="display: inline;">${jobs.jobTitle}</h3>
										<a
											href="${contextPath}/employer/deletejob.htm?jobId=${jobs.jobId}"></a>
									</div>
									<div class="jobContent">
										<span style="font-weight: bold;">${jobs.employer.empOrg.organizationName}</span>
									</div>
									<div class="jobContent">
										<span>${jobs.jobType}</span>
										<form:form action="${contextPath}/student/viewjobdetail.htm"
											cssClass="form-inline">
											<input type="hidden" id="jobId" name="jobId"
												value="${jobs.jobId}" />
											<button type="submit" class="btn btn-info"
												style="padding: 3px 6px;">View Details</button>
										</form:form>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>