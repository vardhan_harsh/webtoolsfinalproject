<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="employer-nav-bar.jsp" />
	<div class="limiter">

		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<h2>Applicant's Applications</h2>
				<hr>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Student Name</th>
							<th>Applied Job</th>
							<th>Applied Date</th>
							<th>Student Message</th>
							<th>Employer's Message</th>
							<th>Action</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empAppList.size() == 0}">
								<tr>
									<td colspan="7">No Applications found</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="application" items="${empAppList}">
									<tr>
										<td>${application.student.firstName}
											${application.student.lastName}</td>
										<td>${application.job.jobTitle}</td>
										<td>${application.dateOfApplication}</td>
										<td>${application.message}</td>
										<td>${application.empmessage}</td>
										<td>${application.status}</td>

										<td><form:form
												action="${contextPath}/employer/student/viewprofile.htm"
												class="appview">
												<input type="hidden" id="userName" name="userName"
													value="${application.student.userName}" />
												<input type="hidden" id="appId" name="appId"
													value="${application.jobApplicationId}" />
												<button type="submit" class="btn btn-primary"
													style="display: inline;">View Details</button>
											</form:form></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>