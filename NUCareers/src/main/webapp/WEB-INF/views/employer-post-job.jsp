<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="employer-nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<c:if test="${message !=null}">
					<div class="alert alert-success fade in"
						style="text-align: center;">
						<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong>
						${message}
					</div>
				</c:if>
				<form:form cssClass="form-horizontal" modelAttribute="job"
					action="${contextPath}/employer/postjob.htm" method="post">
					<div class="form-group">
						<h2 class="control-label col-xs-3" style="text-align: left;">Job
							Posting</h2>
					</div>
					<hr>
					<div class="form-group">
						<label class="control-label col-xs-2">Job title</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputjobTitle"
								path="jobTitle" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword" class="control-label col-xs-2">Job
							Description</label>
						<div class="col-xs-7">
							<form:textarea cssClass="form-control" name="inputjobDesc"
								path="jobDesc" required="required"></form:textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-2">Job Category</label>
						<div class="col-xs-7">
							<form:select path="jobCategory" cssClass="form-control"
								required="required">
								<form:option value="" label="--- Select ---" />
								<form:option value="Customer Support" label="Customer Support" />
								<form:option value="Developer" label="Developer" />
								<form:option value="Information Technology"
									label="Information Technology" />
								<form:option value="Other" label="Other" />
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputfirstname" class="control-label col-xs-2">Job
							Type</label>
						<div class="col-xs-7">
							<form:select path="jobType" cssClass="form-control"
								required="required">
								<form:option value="" label="--- Select ---" />
								<form:option value="Intern" label="Intern" />
								<form:option value="Full-Time" label="Full-Time" />
								<form:option value="Part-Time" label="Part-Time" />
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputjobLocation" class="control-label col-xs-2">Job
							Location</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputjobLocation"
								path="jobLocation" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputsalary" class="control-label col-xs-2">Salary</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputsalary"
								path="salary" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputexperienceLevel" class="control-label col-xs-2">Experience
							Level</label>
						<div class="col-xs-7">
							<div class="radio">
								<label><form:radiobutton path="experienceLevel"
										value="Junior" label="Junior" required="required"></form:radiobutton></label>
							</div>
							<div class="radio">
								<label><form:radiobutton path="experienceLevel"
										value="Senior" label="Senior"></form:radiobutton></label>
							</div>
							<div class="radio">
								<label><form:radiobutton path="experienceLevel"
										value="Manager" label="Manager"></form:radiobutton></label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputtotalExperience" class="control-label col-xs-2">Total
							Experience</label>
						<div class="col-xs-7">
							<div class="radio">
								<label><form:radiobutton path="totalExperience"
										value="0-5" label="0-5" required="required"></form:radiobutton></label>
							</div>
							<div class="radio">
								<label><form:radiobutton path="totalExperience"
										value="5-10" label="5-10"></form:radiobutton></label>
							</div>
							<div class="radio">
								<label><form:radiobutton path="totalExperience"
										value="10+" label="10+"></form:radiobutton></label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputdegree" class="control-label col-xs-2">Academic
							Degree</label>
						<div class="col-xs-7">
							<div class="radio">
								<label><form:radiobutton path="degree"
										value="Bacherlor's Degree" label="Bacherlor's Degree"
										required="required"></form:radiobutton></label>
							</div>
							<div class="radio">
								<label><form:radiobutton path="degree"
										value="Master's Degree" label="Master's Degree"></form:radiobutton></label>
							</div> 
							<div class="radio">
								<label><form:radiobutton path="degree"
										value="Doctorate Degree" label="Doctorate Degree"></form:radiobutton></label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputjobduration" class="control-label col-xs-2">Job
							Duration</label>
						<div class="col-xs-7">
							<form:input type="text" cssClass="form-control"
								name="jobduration" path="jobduration" required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputterm" class="control-label col-xs-2">Term</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputterm" path="term"
								required="required"></form:input>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-offset-2 col-xs-10">
							<button type="submit" class="btn btn-primary m-t-20">Submit</button>
						</div>
					</div>
				</form:form>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>