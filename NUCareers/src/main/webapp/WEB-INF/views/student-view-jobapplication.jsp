<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"> --%>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
<style>
ol.progtrckr {
	margin: 0;
	padding: 0;
	list-style-type
	none;
}

ol.progtrckr li {
	display: inline-block;
	text-align: center;
	line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li {
	width: 49%;
}

ol.progtrckr[data-progtrckr-steps="3"] li {
	width: 33%;
}

ol.progtrckr[data-progtrckr-steps="4"] li {
	width: 24%;
}

ol.progtrckr[data-progtrckr-steps="5"] li {
	width: 19%;
}

ol.progtrckr[data-progtrckr-steps="6"] li {
	width: 16%;
}

ol.progtrckr[data-progtrckr-steps="7"] li {
	width: 14%;
}

ol.progtrckr[data-progtrckr-steps="8"] li {
	width: 12%;
}

ol.progtrckr[data-progtrckr-steps="9"] li {
	width: 11%;
}

ol.progtrckr li.progtrckr-done {
	color: black;
	border-bottom: 4px solid yellowgreen;
}

ol.progtrckr li.progtrckr-todo {
	color: silver;
	border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
	content: "\00a0\00a0";
}

ol.progtrckr li:before {
	position: relative;
	bottom: -2.5em;
	float: left;
	left: 50%;
	line-height: 1em;
}

ol.progtrckr li.progtrckr-done:before {
	content: "\2713";
	color: white;
	background-color: yellowgreen;
	height: 2.2em;
	width: 2.2em;
	line-height: 2.2em;
	border: none;
	border-radius: 2.2em;
}

ol.progtrckr li.progtrckr-todo:before {
	content: "\039F";
	color: silver;
	background-color: white;
	font-size: 2.2em;
	bottom: -1.2em;
}

.popover {
	min-width: 950px !important;
}

.popover-content {
	padding-top: 0px;
	padding-bottom: 25px;
}
</style>
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="student-nav-bar.jsp" />
	<div class="limiter">

		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<h2>Applicant's Applications</h2>
				<hr>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Company Name</th>
							<th>Applied Job</th>
							<th>Applied Date</th>
							<th>Employer's Message</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${appList.size() == 0}">
								<tr>
									<td colspan="5">No Applications found</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="application" items="${appList}">
									<tr>
										<td>${application.job.employer.empOrg.organizationName}</td>
										<td>${application.job.jobTitle}</td>
										<td>${application.dateOfApplication}</td>
										<td>${application.empmessage}</td>
										<td>${application.status}<span
											class="glyphicon glyphicon-info-sign" class="statusPopover"
											style="color: blue; margin-left: 3px; font-size: 16px"
											data-toggle="popover"></span></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div hidden="true" id="popover-content" style="padding-top: 0px">
					<ol id="jobStatusList" class="progtrckr" data-progtrckr-steps="4">
						<li class="progtrckr-todo" value="Applied">Application
							Submitted</li>

						<li class="progtrckr-todo" value="UnderReview">Under Review</li>

						<li class="progtrckr-todo" value="InterviewScheduled">Interview
							Scheduled</li>

						<li class="progtrckr-todo" value="JobOffered">Job Offered</li>
					</ol>
				</div>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
	<script type="text/javascript">
		$("[data-toggle=popover]")
				.popover(
						{
							title : function() {
								var status = $(this).parent()[0].innerText
										.replace(" ", "");
								if (status == "Revoke") {
									return "<span style='font-weight:bold'>Job offer revoked</span>";
								} else if (status == "Rejected") {
									return "<span style='font-weight:bold'>No longer under consideration</span>";
								} else {
									return "<span style='font-weight:bold'>Progress</span>";
								}
							},
							html : true,
							trigger : 'hover',
							placement : 'top',
							container : 'body',
							content : function() {
								var status = $(this).parent()[0].innerText
										.replace(" ", "");
								var listItems = $("#jobStatusList li");
								$('#popover-content').find('span').remove()
								$("#jobStatusList>li.progtrckr-done")
										.removeClass('progtrckr-done');
								$("#jobStatusList>li").addClass(
										'progtrckr-todo');
								if (status == "Revoke") {
									return "<span style='vertical-align:middle'>Did not clear background check</span>";
								} else if (status == "Rejected") {
									return $('#popover-content').html();
								} else {

									listItems.each(function(idx, li) {
										var stat = $(li);
										stat.removeClass('progtrckr-todo');
										stat.addClass('progtrckr-done');
										var liStatus = stat.attr("value")
												.replace(" ", "");
										if (liStatus === status) {
											return false;
										}
									});
									return $('#popover-content').html();
								}

							}
						});
	</script>
</body>
</html>