<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"> --%>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="employer-nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<h1>Job Details</h1>
				<hr>
				<table class="table table-striped">
					<tbody>

						<tr>
							<th scope="row">Company Name</th>
							<td>${job.employer.empOrg.organizationName}</td>
						</tr>

						<tr>
							<th scope="row">Job Title</th>
							<td>${job.jobTitle}</td>
						</tr>

						<tr>
							<th scope="row">Job Description</th>
							<td>${job.jobDesc}</td>
						</tr>

						<tr>
							<th scope="row">Job Location</th>
							<td>${job.jobLocation}</td>
						</tr>

						<tr>
							<th scope="row">Salary</th>
							<td>${job.salary}</td>
						</tr>

						<tr>
							<th scope="row">Job Duration</th>
							<td>${job.jobduration}</td>
						</tr>
						<tr>
							<th scope="row">Job Type</th>
							<td>${job.jobType}</td>
						</tr>
						<tr>
							<th scope="row">Job Term</th>
							<td>${job.term}</td>
						</tr>
					</tbody>
				</table>
				<div class=row>
					<div class=row-col-12>
						<a href="#myModal" class="btn btn-md btn-primary"
							data-toggle="modal" style="float: right;">Edit</a>
					</div>
				</div>

				<!-- Modal HTML -->
				<div id="myModal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<form:form cssClass="form-horizontal" modelAttribute="job"
								action="${contextPath}/employer/updatejob.htm" method="post">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"
										aria-hidden="true">&times;</button>
									<h4 class="modal-title">Edit job details</h4>
								</div>
								<div class="modal-body">


									<div class="form-group">
										<label class="control-label col-xs-3">Company Name</label>
										<div class="col-xs-9">
											<form:input cssClass="form-control" name="inputConmpany"
												path="employer.empOrg.organizationName" readonly="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-xs-3">Job title</label>
										<div class="col-xs-9">
											<form:input cssClass="form-control" name="inputjobTitle"
												path="jobTitle" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputPassword" class="control-label col-xs-3">Job
											Description</label>
										<div class="col-xs-9">
											<form:textarea cssClass="form-control" name="inputjobDesc"
												path="jobDesc" required="true"></form:textarea>
										</div>
									</div>
									<div class="form-group">
										<label for="inputjobLocation" class="control-label col-xs-3">Job
											Location</label>
										<div class="col-xs-9">
											<form:input cssClass="form-control" name="inputjobLocation"
												path="jobLocation" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputsalary" class="control-label col-xs-3">Salary</label>
										<div class="col-xs-9">
											<form:input cssClass="form-control" name="inputsalary"
												path="salary" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputjobduration" class="control-label col-xs-3">Job
											Duration</label>
										<div class="col-xs-9">
											<form:input type="text" cssClass="form-control"
												name="jobduration" path="jobduration" required="true"></form:input>
										</div>
									</div>
									<div class="form-group">
										<label for="inputfirstname" class="control-label col-xs-3">Job
											Type</label>
										<div class="col-xs-9">
											<form:select path="jobType" cssClass="form-control"
												required="true">
												<form:option value="" label="--- Select ---" />
												<form:option value="Intern" label="Intern" />
												<form:option value="Full-Time" label="Full-Time" />
												<form:option value="Part-Time" label="Part-Time" />
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<label for="inputterm" class="control-label col-xs-3">Term</label>
										<div class="col-xs-9">
											<form:input cssClass="form-control" name="inputterm"
												path="term" required="true"></form:input>
										</div>
									</div>

								</div>
								<div class="modal-footer">
									<input type="hidden" value="${job.jobId }" name="jobId" />
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Save
										changes</button>
								</div>
							</form:form>
						</div>
					</div>
				</div>


			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>