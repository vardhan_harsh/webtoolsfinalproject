<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
<style type="text/css">
input[type=file] {
	position: absolute;
	z-index: 2;
	top: 0;
	left: 0;
	filter: alpha(opacity = 0);
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	opacity: 0;
	background-color: transparent;
	color: transparent;
}
</style>
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="student-nav-bar.jsp" />
	<div class="limiter">
		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<c:if test="${message !=null}">
					<div class="alert alert-success fade in"
						style="text-align: center;">
						<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong>
						${message}
					</div>
				</c:if>
				<!-- Upload resume -->
				<form method="POST" action="${contextPath}/student/uploadresume.htm"
					enctype="multipart/form-data" class="form-horizontal">
					<div class="form-group">
						<h2 class="control-label" style="text-align: left;">Student
							Portfolio</h2>
					</div>
					<hr>
					<div class="form-group">
						<label for="path" class="control-label col-xs-2">Select
							file</label>
						<div class="col-xs-7">
							<div style="position: relative;">
								<a class='btn btn-primary' href='javascript:;'
									style="text-decoration: none;"> Browse <input type="file"
									name="file" size="40"
									onchange='$("#upload-file-info").html($(this).val());'>
								</a> &nbsp; <span class='label label-info' id="upload-file-info"></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="path" class="control-label col-xs-2">Name</label>
						<div class="col-xs-4">
							<input type="text" name="name" required="required"
								class="form-control" />
						</div>
					</div>
					<c:if test="${sessionScope.name != null }">
						<div class="form-group">
							<div class="col-xs-7 col-xs-offset-2">
								<c:set var="search" value="\\" />
								<c:set var="myContent" value="${student.filePath}" />
								<c:set var="replace" value="/" />
								<c:set var="myContent"
									value="${fn:replace(myContent, search, replace)}" />
								<a href="${contextPath}/student/download.htm?name=${myContent}"
									target="_blank">${student.fileName}</a>
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<div class="col-xs-offset-2 col-xs-10">
							<button type="submit" class="btn btn-primary">Upload</button>
						</div>
					</div>
				</form>
				<hr>
				<!-- Portfolio -->
				<form:form cssClass="form-horizontal" modelAttribute="student"
					action="${contextPath}/student/submitportfolio.htm" method="post">
					<div class="form-group">
						<label for="inputdegree" class="control-label col-xs-2">Academic
							Degree</label>
						<div class="col-xs-7">
							<div class="radio">
								<label><form:radiobutton path="degree"
										value="Bacherlor's Degree" label="Bacherlor's Degree"></form:radiobutton></label>
							</div>
							<div class="radio">
								<label><form:radiobutton path="degree"
										value="Master's Degree" label="Master's Degree"></form:radiobutton></label>
							</div>
							<div class="radio">
								<label><form:radiobutton path="degree"
										value="Doctorate Degree" label="Doctorate Degree"></form:radiobutton></label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputMajor" class="control-label col-xs-2">Major</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputMajor"
								path="major"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputgpa" class="control-label col-xs-2">GPA</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputjobLocation"
								path="gpa"></form:input>
						</div>
						<label for="inputOut" class="control-label col-xs-2"
							style="text-align: left;">Out of 4.0</label>
					</div>

					<div class="form-group">
						<label for="inputIndustry" class="control-label col-xs-2">Preferred
							Industry</label>
						<div class="col-xs-7">
							<form:input cssClass="form-control" name="inputIndustry"
								path="preferedIndustry"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmployer" class="control-label col-xs-2">Work
							Experience</label>
						<div class="col-xs-7">
							<form:input type="number" cssClass="form-control"
								name="inputEmployer" path="workExperience"></form:input>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-offset-2 col-xs-7">
							<a href="${contextPath}/student/viewprofile.htm"
								class="btn btn-primary" style="float: left;">Back</a>
							<button type="submit" class="btn btn-primary"
								style="float: right;">Submit</button>
						</div>
					</div>
				</form:form>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>
</body>
</html>