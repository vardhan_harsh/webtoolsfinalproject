<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Northeastern University</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/icons/favicon.ico"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/main.css"/>">
<!--===============================================================================================-->
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<jsp:include page="nav-bar.jsp" />
	<jsp:include page="student-nav-bar.jsp" />
	<div class="limiter">

		<div class="content-main">
			<section class="contain-section p-t-30 p-b-30 p-l-30 p-r-30">
				<h1>Job application Form</h1>
				<hr>
				<form:form action="${contextPath }/student/submitjobapplication.htm"
					class="form-horizontal" method="post" modelAttribute="application">
					<div class="form-group">
						<div class="col-xs-10">
							<h2>${job.jobTitle}</h2>
							<h4>Job&nbsp;Id:&nbsp;${job.jobId}&nbsp;-&nbsp;${job.employer.empOrg.organizationName}</h4>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<label for="message" class="col-sm-1 control-label">Message</label>
						<div class="col-xs-4">
							<form:textarea path="message" class="form-control" id="message"
								placeholder="Enter Personal Message" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-1 col-xs-4">
							<input type="hidden" id="jobId" name="jobId" value="${job.jobId}" />
							<button type="submit" class="btn btn-primary"
								style="float: right;">Submit Application</button>
						</div>
					</div>
				</form:form>
				<form:form action="${contextPath}/student/viewjobdetail.htm"
					cssClass="form-inline"
					style="position: relative;top: -48px;display:inline">
					<input type="hidden" id="jobId" name="jobId" value="${job.jobId}" />
					<button type="submit" class="btn btn-primary">Back</button>
				</form:form>
			</section>
		</div>
	</div>
	<!--===============================================================================================-->
	<script
		src="<c:url value="/resources/vendor/jquery/jquery-3.2.1.min.js"/>"></script>
	<!--=============================================================================================== -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="/resources/js/main.js"/>"></script>

</body>
</html>